figure
plot(Puntos,Stree_radius(:,1),Puntos,Stree_radius(:,2),Puntos,Stree_radius(:,3),Puntos,Stree_radius(:,4),Puntos,Stree_radius(:,5))
legend('show','Location','northwest')
xlabel('Puntos searching')
ylabel('Milisegundos')
lgd=legend('0.10','0.15','0.20','0.25','0.30')
title(lgd,'Radio')
figure
plot(Puntos,Stree_k(:,1),Puntos,Stree_k(:,2),Puntos,Stree_k(:,3),Puntos,Stree_k(:,4))
legend('show','Location','northwest')
xlabel('Puntos searching')
ylabel('Milisegundos')
lgd=legend('10','20','30','40')
title(lgd,'Vecinos')
figure
plot(Vecinos,Stree_cell(:,3),Vecinos,Utree_cell(:,3),Vecinos,ANN_cell(:,3))
legend('show','Location','northwest')
xlabel('Vecinos')
ylabel('Milisegundos')
lgd=legend('Stree','Utree','Ann')
title(lgd,'Tipo')

