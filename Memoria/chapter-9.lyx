#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
% increases link area for cross-references and autoname them
% if you change the document language to e.g. French
% you must change "extrasenglish" to "extrasfrench"
\AtBeginDocument{%
 \renewcommand{\ref}[1]{\mbox{\autoref{#1}}}
}
\def\refnamechanges{%
 \renewcommand*{\equationautorefname}[1]{}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
\@ifpackageloaded{babel}{\addto\extrasenglish{\refnamechanges}}{\refnamechanges}

% in case somebody want to have the label "Equation"
%\renewcommand{\eqref}[1]{Equation~(\negthinspace\autoref{#1})}

% that links to image floats jumps to the beginning
% of the float and not to its caption
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% makes caption labels bold
% for more info about these settings, see
% http://mirrors.ctan.org/macros/latex/contrib/koma-script/doc/scrguien.pdf
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enables calculations
\usepackage{calc}

% fancy page header/footer settings
% for more information see section 9 of
% ftp://www.ctan.org/pub/tex-archive/macros/latex2e/contrib/fancyhdr/fancyhdr.pdf
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increases the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoids that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument 1
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Planificación temporal y presupuesto
\begin_inset CommandInset label
LatexCommand label
name "chap:Planificación-temporal-y"

\end_inset


\end_layout

\begin_layout Standard
En este capítulo se detallará la gestión de los recursos, temporales y materiale
s empleados en la realización del trabajo de fin de grado.
\end_layout

\begin_layout Section
Planificación temporal
\end_layout

\begin_layout Standard
Se considera como fecha de comienzo del presente trabajo fin de grado, el
 27 de Septiembre de 2016, día en el que se realiza la primera reunión sobre
 el mismo, finalizando el 13 de Julio de 2017, día en el que se termina
 de redactar esta memoria.
\end_layout

\begin_layout Standard
Si bien las fechas de comienzo del calendario 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Calendario"

\end_inset

 son las reales, las fechas de finalización mostradas son aquellas que se
 habrían podido alcanzar de haberse dedicado la totalidad de un horario
 laboral normal al desarrollo de este.
 Debido que su desarrollo se ha compaginado con los estudios, el trabajo
 se ha realizado en varias etapas, con las correspondientes pausas durante
 época de exámenes:
\end_layout

\begin_layout Itemize
Primer semestre (Septiembre de 2016 a Noviembre de 2016): Introducción al
 proyecto, familiarización con LYX, los 
\emph on
Kd-Trees
\emph default
, Git y Ubuntu (Linux).
\end_layout

\begin_layout Itemize
Segundo semestre (Febrero de 2017 a Marzo de 2017): Aprendizaje y transición
 de C a C
\family typewriter
++
\family default
 y desarrollo de los algoritmos de búsqueda de radio fijo y 
\emph on
k 
\emph default
vecinos para cualquier dimensión mediante 
\emph on
Kd-Trees
\emph default
.
\end_layout

\begin_layout Itemize
Segundo semestre (Abril de 2017 a Julio de 2017): Integración de los árboles
 a bajo nivel y desarrollo de los algoritmos de alto nivel.
 Comparativas y redacción de la memoria.
\end_layout

\begin_layout Standard
\noindent
Haciendo un total de 
\series bold
316
\series default
 horas.
 Pudiéndose apreciar el desglose en tareas de estas etapas en el 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Diagrama-de-Gantt"

\end_inset

.
\end_layout

\begin_layout Standard
\noindent
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename Imagenes/Gantt.png
	lyxscale 70
	scale 70
	rotateAngle 90

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Diagrama de Gantt
\begin_inset CommandInset label
LatexCommand label
name "fig:Calendario"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\noindent
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename Imagenes/Quesitos.PNG
	lyxscale 75
	scale 75

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Distribución temporal
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Newpage clearpage
\end_inset


\end_layout

\begin_layout Section
Presupuesto
\end_layout

\begin_layout Standard
En este apartado se procede a detallar la valoración económica del trabajo
 de fin de grado realizado.
 Al tratarse de un proyecto de naturaleza teórica, el presupuesto de ejecución
 material coincide con el presupuesto total, siendo las partidas consideradas:
 
\end_layout

\begin_layout Itemize
Los gastos de personal precisado en este proyecto.
\end_layout

\begin_layout Itemize
El coste de las licencias del software empleado.
\end_layout

\begin_layout Itemize
El coste de los equipos materiales empleados.
\end_layout

\begin_layout Standard
Los costes de personal han sido más difíciles de cuantificar con precisión.
 Para ello, se ha decidido asignar un sueldo en bruto acorde a un ingeniero
 sénior para el tutor y acorde a un empleado en prácticas para el estudiante,
 siguiendo los estándares propios del sector.
 Sólo se han considerado la horas empleadas en tutorías o trabajo en común
 a la hora contabilizar el tiempo dedicado por el tutor al trabajo.
 No se han contabilizado las horas empleadas por el mismo en el desarrollo
 de diferentes partes de la implementación.
\end_layout

\begin_layout Standard
Para el cálculo de la amortización se ha supuesto una depreciación lineal
 a 4 años.
 Siendo el material de trabajo empleado un ordenador portátil valorado en
 500€, y habiendo durado el proyecto 10 meses, resulta de un valor de 125
 €.
\end_layout

\begin_layout Standard
Ascendiendo el presupuesto general a la cantidad de SEIS MIL OCHOCIENTOS
 CINCUENTA Y DOS EUROS CON OCHENTA Y DOS CÉNTIMOS.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Graphics
	filename Imagenes/Presupuesto.PNG
	rotateAngle 90

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Presupuesto
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
