#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
% increases link area for cross-references and autoname them
% if you change the document language to e.g. French
% you must change "extrasenglish" to "extrasfrench"
\AtBeginDocument{%
 \renewcommand{\ref}[1]{\mbox{\autoref{#1}}}
}
\def\refnamechanges{%
 \renewcommand*{\equationautorefname}[1]{}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
\@ifpackageloaded{babel}{\addto\extrasenglish{\refnamechanges}}{\refnamechanges}

% in case somebody want to have the label "Equation"
%\renewcommand{\eqref}[1]{Equation~(\negthinspace\autoref{#1})}

% that links to image floats jumps to the beginning
% of the float and not to its caption
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% makes caption labels bold
% for more info about these settings, see
% http://mirrors.ctan.org/macros/latex/contrib/koma-script/doc/scrguien.pdf
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enables calculations
\usepackage{calc}

% fancy page header/footer settings
% for more information see section 9 of
% ftp://www.ctan.org/pub/tex-archive/macros/latex2e/contrib/fancyhdr/fancyhdr.pdf
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increases the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoids that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language french
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument 1
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Introducción
\begin_inset CommandInset label
LatexCommand label
name "chap:Introducción"

\end_inset


\end_layout

\begin_layout Standard
En este capítulo se explicará la motivación que ha llevado a la realización
 de este trabajo de fin de grado, así como los objetivos que se pretenden
 cumplir con la realización del mismo.
\end_layout

\begin_layout Section*
Motivación personal
\end_layout

\begin_layout Standard
La elección del tema y contenido del presente trabajo de fin de grado provienen
 de mi deseo de adquirir la capacidad de programar en lenguajes de alto
 de nivel y conocer la aplicación de los mismos a problemas de la industria
\begin_inset Note Note
status open

\begin_layout Plain Layout
(HECHO) stf: yo no diría escala
\end_layout

\end_inset

 real.
 
\end_layout

\begin_layout Standard
La preparación teórica necesaria para la realización de este trabajo la
 he podido ir adquiriendo durante la realización del mismo, pues el problema
 de la vecindad de puntos es principalmente un problema de implementación.
 Mi esfuerzo se ha concentrado en la adquisición de las habilidades y conocimien
tos necesarios para el uso de lenguajes de alto nivel, tal y como deseaba,
 sirviendo la realización del trabajo no sólo como una introducción a la
 investigación en este campo, sino también como un complemento a mi formación.
\end_layout

\begin_layout Standard
Con la guía y apoyo del tutor he podido acceder al problema de forma gradual,
 incrementando escalonadamente la dificultad de los problemas a resolver
 y adquiriendo poco a poco la capacidad de afrontar nuevos retos.
 
\end_layout

\begin_layout Standard
Creo en muchos sentidos que he escogido el mejor trabajo de fin grado posible
 para mi desarrollo académico en este campo pues el problema de la vecindad
 de puntos trata alguna de las áreas fundamentales de las ciencias de la
 computación.
 Estas son, complejidad de algoritmos y estructuras de datos.
 Este hecho me ha permitido introducirme de forma natural en esta rama de
 la ingeniería e ir aprendiendo sobre el tema mientras trabajaba.
 El tener un problema concreto en mente, para cuya resolución es necesaria
 el estudio de estas áreas, ha amenizado mi aprendizaje sobre las mismas.
 Aprendizaje que de otra forma sospecho que puede llegar a ser muy árido
 si se trata únicamente desde un punto de vista teórico.
\end_layout

\begin_layout Standard
En conclusión, me alegro de haber escogido un trabajo que me haya permitido
 aprender sobre las áreas en las que estaba interesado y no por ello dejar
 de estar contribuyendo a una aplicación tan útil para la ingeniería industrial
 como pueden ser los métodos numéricos sin mallado.
\end_layout

\begin_layout Standard
\begin_inset Newpage clearpage
\end_inset


\end_layout

\begin_layout Section*
Objetivos
\begin_inset CommandInset label
LatexCommand label
name "sec:Objetivos"

\end_inset


\end_layout

\begin_layout Standard
Los objetivos fundamentales del trabajo son:
\end_layout

\begin_layout Itemize
Mejora y desarrollo de una implementación existente que soluciona el problema
 de la vecindad de puntos.
\end_layout

\begin_layout Itemize
Comparativa de implementaciones existentes respecto a la elaborada.
\end_layout

\begin_layout Itemize
Análisis de los resultados obtenidos y determinación de casos de uso óptimos.
\end_layout

\begin_layout Standard
La implementación sobre la que se trabaja es una librería de cabeceras desarroll
ada por S.
 Tapia en colaboración con A.
 Beltrán y I.
 Romero 
\begin_inset CommandInset citation
LatexCommand cite
key "Santy"

\end_inset

.
 En concreto, se aprovechará la idea de dividir el espacio en celdas en
 función de la codificación en coma flotante de los puntos del conjunto
 estudiado.
 La mejora significativa que se pretende añadir es dotar a estas celdas
 de un algoritmo de búsqueda a bajo nivel.
 De esta forma se posibilita el uso de diferentes métodos de búsqueda en
 función de las características de los puntos pertenecientes a la celda.
 Se pretende de esta forma acelerar la búsqueda de vecinos del conjunto,
 mejorando los resultados obtenidos.
 Se describe el algoritmo y su implementación en 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Diseño-de-algoritmos"

\end_inset

 y 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Detalles-de-implementación"

\end_inset

.
\end_layout

\begin_layout Standard
Se comparará con librerías existentes como 
\begin_inset CommandInset citation
LatexCommand cite
key "ANN"

\end_inset

, cuya elección se discute en 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Implementaciones-existentes"

\end_inset

.
 Y se discutirán los resultados obtenidos en 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Benchmarking-(o-comparativas)"

\end_inset

 y 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Conclusiones-y-futuros"

\end_inset

.
 
\end_layout

\begin_layout Standard
Para el cumplimiento de estos objetivos se ha seguido el siguiente procedimiento
:
\end_layout

\begin_layout Enumerate
Adquisición de los conocimientos y habilidades necesarios para el uso de
 un lenguaje de alto nivel, así como de las herramientas necesarias para
 ello.
\end_layout

\begin_layout Enumerate
Estudio de implementaciones existentes y del trabajo en el que se basa la
 solución propuesta que se pretende mejorar.
\end_layout

\begin_layout Enumerate
Diseño del algoritmo y su implementación.
\end_layout

\begin_layout Enumerate
Prueba exhaustiva de los algoritmos con múltiples bancos de pruebas para
 asegurar su correcto funcionamiento.
\end_layout

\begin_layout Enumerate
Asegurar la viabilidad de la implementación para su paralelización en múltiples
 núcleos procesadores.
\end_layout

\begin_layout Enumerate
Comparación con otras implementaciones existentes y estudio de resultados.
\end_layout

\begin_layout Standard
Se muestra en 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Planificación-temporal-y"

\end_inset

 la distribución temporal del trabajo y los recursos empleados para la realizaci
ón del mismo.
\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
