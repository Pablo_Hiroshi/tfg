int select_median(int start, int end, const unsigned axis)
{
    int i, pidx, median = start + (end-start)/2;
    double pivot;
    while(1){
        if ( start == end ) return start;
        pivot = this->point_vector[median][axis];

        //Partition algorithm
        this->swap(median, end); // Move pivot to end
        for ( pidx = i = start; i < end - 1; ++i )
        {
            if (  this->point_vector[i][axis] <= pivot )
            {
                if( i != pidx ) this->swap(pidx, i);
                ++pidx;
            }
        }
        this->swap(end, pidx); // Move pivot to its final place
        //End of partition
        if ( pidx == end ) return pidx;
        if ( end  <  pidx  ) end = pidx - 1;
        else start = pidx + 1;
    }
}