query_result& query(const std::array<double, DIM>& querying, double sq_radius) override
{
    // Clear previous results
    this->result.clear();
    size_t i = 0;
    for ( const auto& p : this->point_vector )
    {
        if ( p.norm( querying ) < sq_radius )
        {
            this->result.push_back( this->ref_vector[i] );
        }
        ++i;
    }
    return this->result;
}