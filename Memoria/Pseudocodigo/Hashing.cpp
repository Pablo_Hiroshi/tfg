namespace std
{
    template< unsigned DIM > struct hash< tessel<DIM> >
    {
        typedef tessel<DIM> argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& k) const
        {
            std::hash<long long> hash_f;
            size_t h = 0;

            h =  hash_f(k.coor[0]);
            // Magic Number and Expression adapted from boost::hash_combine
            unsigned i;
            for ( i = 1; i < DIM; ++i ) {
                h ^= hash_f(k.coor[i]) + 0x9e3779b9 + (h<<6) + (h>>2);
            }
            return h;
        }
    };
}