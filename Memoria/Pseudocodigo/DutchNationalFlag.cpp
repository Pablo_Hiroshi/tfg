int partition (int start, int end, const unsigned axis, double pivot_value){
    int left = start;
    int mid = start;
        int right = end;
    while ( mid <= right )
    {
        if( this->point_vector[mid][axis] < pivot_value )
        {
            if( left != mid ) this->swap(left, mid);
            ++left;
            ++mid;
        }
        else if ( this->point_vector[mid][axis] > pivot_value )
        {
            if( mid != right ) this->swap(mid, right);
            --right;
        }
        else ++mid;
    }
    if( right < start ) return start;
    else return right;
}