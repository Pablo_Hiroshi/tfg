int build_recursive(int start, int len, unsigned axis) override
{
    if( len <= 0 ) return -1;

    int end = start + len - 1;
    int pivot = start + ( end - start )/2;

    pivot = partition(start, end, axis, this->point_vector[pivot][axis]);

    if ( ++axis >= DIM ) axis = 0;
    this->point_vector[pivot].left = build_recursive(start, pivot - start, axis);
    this->point_vector[pivot].right = build_recursive(pivot + 1, start + len - (pivot + 1), axis);
    return pivot;
}