void tessel_query(const std::array<double, DIM>& coord, const double sq_radius, const tessel<DIM>& c, tessel<DIM>& aux,
                    std::vector<uintptr_t>& result, long x[DIM], unsigned i, const long r ) const
{
    if( i < DIM )
    {
        for ( x[i]= -r; x[i] <= r; ++x[i]) {
            tessel_query( coord, sq_radius, c, aux, result, x, i + 1, r );
        }
    }
    else
    {
        unsigned j;
        for( j = 0; j < DIM; ++j){
            aux.coor[j] = c.coor[j] + x[j];
        }

        typename HashMap::const_iterator found = domain.find(aux);
        if ( found != domain.end() )
        {
            query_result q_result = found->second->query(coord, sq_radius);
            result.reserve(result.size()+q_result.size());
            result.insert(result.end(), q_result.begin(), q_result.end());
        }
    }
}