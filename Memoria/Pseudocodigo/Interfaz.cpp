/* first = norm, second = reference */
typedef std::pair< double, uintptr_t > distance_and_ref;

typedef std::unordered_map< uintptr_t, std::vector<uintptr_t> > multiquery_result_t;
typedef std::unordered_map< uintptr_t, std::vector<distance_and_ref> > multi_K_query_result_t;


template<typename Iterator, typename type_of_set>
void insert(type_of_set& the_set, Iterator first, Iterator last);

template<typename Iterator, typename type_of_set>
multiquery_result_t query(type_of_set& the_set, double radius, Iterator first, Iterator last);

template<typename Iterator, typename type_of_set>
multi_K_query_result_t kquery(type_of_set& the_set, size_t K, Iterator first, Iterator last);



