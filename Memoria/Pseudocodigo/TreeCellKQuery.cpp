void kquery_recursive (int index, const std::array<double, DIM>& querying, const int K,
                        unsigned& max_idx, double& max_dist, double& x_max_dist, unsigned axis)
{
    if ( index == -1 ) return;

    double d, dx, dx2;
    d = point_vector[index].norm( querying );
    distance_and_ref curr_point = { d, ref_vector[index] };

    unsigned stored = norm_and_refs.size();
    if( stored < K )
    {
        norm_and_refs.push_back( curr_point );
        if( d > max_dist ){
            max_dist = x_max_dist = d;
            max_idx = stored;
        }
        stored++;
    }
    else if( d == max_dist )
    {
        norm_and_refs.push_back( curr_point );
        x_max_dist = d;
    }
    else if ( d < max_dist )
    {
        if( max_dist == x_max_dist ) norm_and_refs.push_back( norm_and_refs[max_idx] );
        norm_and_refs[max_idx] = curr_point;
        max_dist = 0;
        int i;
        for( i = 0; i < K; ++i )
        {
            /* Tag the largest distance */
            if( norm_and_refs[i].first > max_dist){
                max_dist = norm_and_refs[i].first;
                max_idx = i;
            }
        }
        if( max_dist < x_max_dist )
        {
            norm_and_refs.resize ( K );
            x_max_dist = max_dist;
        }
    }

    dx = point_vector[index][axis] - querying[axis];
    dx2 = dx * dx;
    ++axis;
    if ( axis >= DIM ) axis = 0;
    kquery_recursive(dx > 0 ? point_vector[index].left : point_vector[index].right, querying, K, max_idx, max_dist, x_max_dist, axis);
    if ( dx2 > max_dist && stored == K) return;
    kquery_recursive(dx > 0 ? point_vector[index].right : point_vector[index].left, querying, K, max_idx, max_dist, x_max_dist, axis);
}