inline long long bit_lattice(double x, int range)
{
    if ( x > 0 && x < exp2(range) )
    {
        return 0;
    }
    else if ( x < 0 && -x < exp2(range) )
    {
        return -1;
    }

    const long long unsigned SIGN_BIT = 0x8000000000000000Lu;
    const long long unsigned EXPO_BIT = 0x7FF0000000000000Lu;
    const long long unsigned CAND_BIT = 0x000FFFFFFFFFFFFFLu;

    union number {
        double re;
        long long unsigned i;
    };
    union number u;
    u.re = x;    

    int sign = u.i & SIGN_BIT ? 1 : 0;
    long long unsigned exponent = (u.i & EXPO_BIT) >> 52Lu;
    long long unsigned significand = (u.i & CAND_BIT) + (1Lu << 52);
    
     if(!exponent) // If exponent is all zeros, it is denormalized
    {
        return sign? -1 : 0;
    }

    /* significand is *2^52 and exponent exceed is 1023 */
    long long unsigned u_result = significand >> ( 1023 - exponent + range + 52 );
    if ( sign ) /* negative */
    {
        return -1L - (long long)u_result;
    }
    else
    {
        return (long long)u_result;
    }
}