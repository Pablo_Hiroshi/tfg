 void tessel_kquery_3( const std::array<double, DIM>& coord, const tessel<DIM>& c, std::vector<distance_and_ref>& result, const size_t K ) const
{

bool keep_going = true;
double worst_dist = INFINITY;
long x[DIM], r = 0;
tessel<DIM> aux;
while ( keep_going )
{
    r++;
    for ( x[0] = -r; x[0] <= r; ++x[0]  )
    {
        for ( x[1] = -r; x[1] <= r; ++x[1] )
        {
            for ( x[2] = -r; x[2] <= r; ++x[2] )
            {
                if ( abs(x[0]) >= r || abs(x[1]) >= r || abs(x[2]) >= r )
                {
                    aux.coor[0] = c.coor[0] + x[0];
                    aux.coor[1] = c.coor[1] + x[1];
                    aux.coor[2] = c.coor[2] + x[2];
                    typename HashMap::const_iterator found = domain.find(aux);
                    if ( found != domain.end() )
                    {
                        kquery_result q_result = found->second->kquery(coord, K );
                        result.insert(result.end(), q_result.begin(), q_result.end());
                        if ( K <= result.size() )
                        {
                            std::sort(result.begin(), result.end());
                            worst_dist = result[K - 1].first;
                        }
                    }
                }
            }
        }
    }

    keep_going = false;
    if ( result.size() < K )
    {
        keep_going = true;
    }
    else // Copiado de Santiago:
    {
        for ( int i = 0; i < DIM; ++i )
        {
            keep_going |= coord[i] + sqrt(worst_dist) > (c.coor[i] + r) * length;
            keep_going |= coord[i] - sqrt(worst_dist) < (c.coor[i] - r + 1) * length;
        }
    }
}

}
