kquery_result& kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
{
    // Clear previous results
    this->norm_and_refs.clear();
    size_t i = 0;
    for ( const auto& p : this->point_vector )
    {
        distance_and_ref dr;
        dr.first = p.norm( querying );
        dr.second = this->ref_vector[i];
        if ( target_sq_radius < 0 || dr.first < target_sq_radius )
        {
            this->norm_and_refs.push_back( dr );
        }
        ++i;
    }

    std::sort( this->norm_and_refs.begin(), this->norm_and_refs.end() );

    size_t _size = this->norm_and_refs.size();
    size_t number_of = K <=_size ? K : _size; // number_of = min(K, _size)

    // Add more points if they have the same distance:
    while ( number_of < _size && this->norm_and_refs[number_of].first == this->norm_and_refs[number_of-1].first )
    {
        ++number_of;
    }

    this->norm_and_refs.resize(number_of);
    
    return this->norm_and_refs;
}