inline double norm( const std::array<double, DIM>& p2 ) const
{
    double sum = 0; unsigned i;
    for ( i = 0; i < DIM; ++i ) {
        sum += ((*this)[i]-p2[i])*((*this)[i]-p2[i]);
    }
    return sum;
}