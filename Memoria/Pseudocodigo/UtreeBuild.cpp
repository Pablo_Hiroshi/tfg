int build_recursive(int start, int len, unsigned axis) override
{
    if( len <= 0 ) return -1;
    int median;
    int end = start + len - 1;
    /* Find the median in order to build balanced trees*/
    median = select_median(start, end, axis);
    median = unbalance_median(start, end, axis, median);
    if ( ++axis >= DIM ) axis = 0;
    this->point_vector[median].left = build_recursive(start, median - start, axis);
    this->point_vector[median].right = build_recursive(median + 1, start + len - (median + 1), axis);
    return median;
}