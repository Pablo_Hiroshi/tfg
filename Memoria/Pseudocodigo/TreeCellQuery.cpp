void query_recursive (unsigned index, const std::array<double, DIM>& querying, const double sq_radius, unsigned axis)
{
    if ( index == -1 ) return;

    double d, dx, dx2;
    d = point_vector[index].norm( querying );
    dx = point_vector[index][axis] - querying[axis];
    dx2 = dx*dx;

    if ( d < sq_radius ) {
    result.push_back(ref_vector[index]);
    }

    ++axis;
    if ( axis >= DIM ) axis = 0;
    query_recursive(dx > 0 ? point_vector[index].left : point_vector[index].right , querying, sq_radius, axis);
    if ( dx2 > sq_radius ) return;
    query_recursive(dx > 0 ? point_vector[index].right : point_vector[index].left, querying, sq_radius, axis);
}