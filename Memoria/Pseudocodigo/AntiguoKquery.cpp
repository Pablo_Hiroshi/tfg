void kquery_recursive (int index, const double querying[DIM], std::vector<unsigned> &neighbors, std::vector<unsigned> &extra_neighbors, const int K, unsigned *max_idx, double *max_dist, double *extra_max_dist, unsigned axis)
{
    if ( index== -1 ) return;
    double d, dx, dx2;
    d = norm(point_vector[index].p, querying);
    unsigned stored = neighbors.size();

    if( stored < K ){
        neighbors.push_back(index);
        if( d > *max_dist ){
            *max_dist = d;
            *max_idx = stored;
        }
        stored++;
    }
    else if ( d == *max_dist ){
        if( *max_dist < *extra_max_dist ){
            extra_neighbors.clear();
        }
        *extra_max_dist = *max_dist;
        extra_neighbors.push_back( index );
    }
    else if( d < *max_dist ){
        neighbors[*max_idx] = index;
        *max_dist = 0;
        int i;
        for( i = 0; i < K; ++i )
        {   /*Tag the biggest distance*/
            double aux_dist = norm(point_vector[neighbors[i]], querying);
            if( aux_dist > *max_dist){
                *max_dist = aux_dist;
                *max_idx = i;
            }
        }
    }
    /* if chance of exact match is high */
    if ( !*max_dist ) return;
    dx = point_vector[index].p[axis] - querying[axis];
    dx2 = dx * dx; 

    if ( ++axis >= DIM ) axis = 0;
    kquery_recursive(dx > 0 ? point_vector[index].left : point_vector[index].right, querying, neighbors, extra_neighbors, K, max_idx, max_dist, extra_max_dist, axis);
    if ( dx2 > *max_dist ) return;
    kquery_recursive(dx > 0 ? point_vector[index].right : point_vector[index].left, querying, neighbors, extra_neighbors, K, max_idx, max_dist, extra_max_dist, axis);
}