query_result& query(const std::array<double, DIM>& querying, double sq_radius) override
{
    // Clear previous results
    this->result.clear();

    // Trying vectorization:
    size_t i, _size = point_vector.size(), size_v = _size / DIM;
    typedef double (*pointer2vect)[DIM];
    pointer2vect auxv = (pointer2vect) point_aux_vector.data();

    for ( i = 0; i < size_v; ++i ) // Preparing the vector
    {
        memcpy(auxv + i, querying.data(), DIM*sizeof(double));
    }

    double *p   = point_vector.data();
    double *aux = point_aux_vector.data();
    for ( i = 0; i < _size; ++i ) // Distance
    {
        aux[i] -= p[i];
    }

    for ( i = 0; i < _size; ++i ) // Quadratic distance
    {
        aux[i] *= aux[i];
    }

    for ( i = 0; i < _size / DIM; ++i )
    {
        double sum = 0; size_t j;
        for (  j = 0; j < DIM; ++j ) { // Norm
            sum += (*aux);
            ++aux;
        }
        if ( sum < sq_radius ) {
            this->result.push_back( this->ref_vector[i] );
        }
    }

    return this->result;
}