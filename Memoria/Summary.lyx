#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
% enables calculations
\usepackage{calc}

% increases the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Abstract"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 2
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation skip
\defskip bigskip
\quotes_language french
\papercolumns 1
\papersides 2
\paperpagestyle plain
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter*
RESUMEN EJECUTIVO
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
addcontentsline{toc}{chapter}{Resumen Ejecutivo} 
\end_layout

\end_inset


\begin_inset Note Note
status open

\begin_layout Plain Layout
adds table of contents entry
\end_layout

\end_inset


\end_layout

\begin_layout Standard
El objetivo de este trabajo es proponer una solución al problema de la vecindad
 de puntos (
\emph on
neighborhood problem
\emph default
) para su aplicación en métodos numéricos sin mallado (
\emph on
meshfree methods
\emph default
), como el análisis de elementos finitos.
\begin_inset Note Note
status open

\begin_layout Plain Layout
(HECHO)stf: indica ejemplos de métodos, igual no caen, por ejemplo simulación
 de elementos finitos
\end_layout

\end_inset

 Se determinan los rangos y situaciones óptimas de aplicación de la solución,
 así como la comparativa de la misma frente algoritmos existentes.
 
\end_layout

\begin_layout Standard
El problema de la vecindad de puntos o problema de proximidad (
\emph on
nearest neighbor search NNS
\emph default
), es un problema de optimización.
 Consiste en determinar cuántos y qué puntos pertenecientes a un conjunto
 se encuentran suficientemente próximos entre sí como para considerarse
 vecinos.
 Una tarea trivial como puede ser la búsqueda del punto más cercano a otro,
 se puede convertir en una tarea con un coste computacional inaceptable
 cuando el problema escala en búsquedas del orden de cientos de miles dentro
 de un conjunto de millones de puntos.
 Métodos de fuerza bruta que calculan las distancias de cada punto del conjunto
 al punto objetivo, pierden todo valor a gran escala debido a su lentitud,
 por lo que es necesario afrontar el problema mediante otros métodos.
 
\end_layout

\begin_layout Standard
La solución a este problema tiene numerosas aplicaciones en diversos campos
 de la ingeniería, así como en otras ciencias.
 Su generalización permite la aplicación de la misma en tareas tan dispares
 como el tratamiento de imágenes (donde los puntos son píxeles y la distancia
 es el parecido entre colores), la detección de plagios (donde los puntos
 son palabras y la distancia es la similitud entre estas), o métodos numéricos
 (donde los puntos son puntos en el espacio y la distancia es la distancia
 euclídea entre ellos).
\end_layout

\begin_layout Standard
Este trabajo se centra en la resolución del problema para esta última aplicación.
 Escalar problemas de elementos finitos permite calcular y diseñar con mayor
 precisión estructuras o máquinas.
 Por ejemplo, en el campo de la ingeniería, esto se podría traducir en la
 fabricación de aerogeneradores y turbinas más eficientes.
 
\begin_inset Note Note
status open

\begin_layout Plain Layout
(HECHO)stf: pon ejemplo y concreta: especialmente en el cálculo y diseño
 de estructuras o máquinas, tales como un aerogenerador o una turbina,...
\end_layout

\end_inset

 Concretamente, la resolución del problema de la vecindad de puntos es sumamente
 útil en los métodos númericos sin mallado, pues a diferencia del método
 clásico de análisis con mallado, los vecinos de cada punto no son fijos
 y es necesario recalcularlos en cada iteración.
 La ausencia de mallado permite resolver simulaciones de dinámica de fluidos,
 o de grandes deformaciones en sólidos (para materiales con comportamiento
 plástico), donde la situación de cada punto puede variar significativamente
 de una iteración a la siguiente, y es necesario determinar sus puntos vecinos
 de forma dinámica.
\end_layout

\begin_layout Standard
Se propone primero una solución al problema, y posteriormente se comparará
 la misma respecto a otras soluciones existentes.
 En la solución propuesta, se cubren las búsquedas para 
\emph on
d 
\emph default
dimensiones, y se permitirán búsquedas para hallar los puntos dentro un
 radio 
\emph on
r
\emph default
, así como los 
\emph on
k 
\emph default
puntos más próximos del punto objetivo.
 Para ello, se preprocesa dinámicamente el conjunto de datos, dividiendo
 los puntos del espacio en celdas y construyendo en ellas árboles de búsqueda
 binarios en caso de haber suficiente densidad de puntos, optimizando el
 empleo de métodos de fuerza bruta o búsqueda binaria según la celda.
\end_layout

\begin_layout Standard
La solución se compara respecto a las existentes para un conjunto de problemas
 en tres dimensiones y se determina en qué casos cuál es la mejor solución,
 la generalidad de los resultados y su extrapolación teórica a dimensiones
 superiores.
 Así mismo, se validarán las previsiones teóricas del coste computacional
 de la solución propuesta respecto de los resultados obtenidos en la práctica.
\end_layout

\begin_layout Subsection*
CÓDIGOS UNESCO
\end_layout

\begin_layout Standard
110213 FUNCIONES RECURSIVAS
\end_layout

\begin_layout Standard
120324 TEORIA DE LA PROGRAMACION
\end_layout

\begin_layout Standard
120406 GEOMETRIA EUCLIDEA
\end_layout

\begin_layout Standard
120601 CONSTRUCCION DE ALGORITMOS
\end_layout

\begin_layout Chapter*
EXECUTIVE SUMMARY
\end_layout

\begin_layout Standard
The objective of this project is to propose a solution to the neighborhood
 problem and its application to meshfree methods in numerical analysis,
 such as the finite element method.
 This is, to determine its range and optimal situations in which it can
 be applied while comparing the solution to already existing algorithms.
\end_layout

\begin_layout Standard
The nearest neighbor search NNS is an optimization problem.The problem is
 to find the point in a given set that is the closest (or most similar)
 to a given point.
 An easy task such searching the closest point in a set to a given point
 can turn into a computational complexity nightmare when the problem scales
 to multiple querys in big data sets.
 Brute force methods that evaluate the distances between the points in a
 set to the given point, are useless in large problems because of their
 poor performance.
 The problem must be solved with other methods.
\end_layout

\begin_layout Standard
The solution to these problems has multiple applications in the engineering
 field, aswell as in other sciences.
 Its generalization allows it to solve very different tasks such image pocessing
 (being the points pixels, and the distance the similarity of their colours),
 plagiarism detection (being the points words, and the distance the similarity
 between them), or numerical analysis (being the points, points in the space,
 and the distance the euclidean distance between them).
\end_layout

\begin_layout Standard
This project is centered around the solution of the problem for the numerical
 analysis.
 The scaling of finite element methods enables more accurate calculations
 and designs of structures and machines.
 For instance, in the engineering field, this could be translated to more
 efficient turbines.
 Specifically, solving the neighborhood problem is very useful in meshfree
 methods because unlike the classical numerycal analysis with meshes, the
 neighbors are not fixed in the space and they need to be recalculated in
 each iteration.
 The lack of a mesh enables the solving of fluid dynamics simulations, or
 great deformation in solids (for materials with plastic behaviour), where
 the place of each point may vary significantly from one iteration to the
 next, being a must to determine its neighbor points dynamically.
\end_layout

\begin_layout Standard
In the first place, a solution to the problem will be proposed, and then
 it will be compared against other existing solutions.
 The proposed solution allows querys for 
\emph on
d
\emph default
 dimensions, querying the neighbor points within a radius 
\emph on
r
\emph default
, aswell as the 
\emph on
k
\emph default
 nearest points to the given point.
 Because of this, the data set will be preprocessed, dividing the space
 in cells and building binary search trees when there are enough points
 in a cell, optimizing the use of brute force methods or binary search according
 to the number of points in a cell.
\end_layout

\begin_layout Standard
The solution will be compared with existing ones for a set of three dimensional
 problems and the best solution, the generality of the results and its theoretic
al extrapolation will be determined for each case.
 Moreover, the theoretical computational complexity of the solution will
 be validated with the results obtained in practice.
\end_layout

\end_body
\end_document
