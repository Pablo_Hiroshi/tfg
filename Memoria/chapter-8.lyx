#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass scrbook
\begin_preamble
% increases link area for cross-references and autoname them
% if you change the document language to e.g. French
% you must change "extrasenglish" to "extrasfrench"
\AtBeginDocument{%
 \renewcommand{\ref}[1]{\mbox{\autoref{#1}}}
}
\def\refnamechanges{%
 \renewcommand*{\equationautorefname}[1]{}
 \renewcommand{\sectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsectionautorefname}{sec.\negthinspace}
 \renewcommand{\subsubsectionautorefname}{sec.\negthinspace}
 \renewcommand{\figureautorefname}{Fig.\negthinspace}
 \renewcommand{\tableautorefname}{Tab.\negthinspace}
}
\@ifpackageloaded{babel}{\addto\extrasenglish{\refnamechanges}}{\refnamechanges}

% in case somebody want to have the label "Equation"
%\renewcommand{\eqref}[1]{Equation~(\negthinspace\autoref{#1})}

% that links to image floats jumps to the beginning
% of the float and not to its caption
\usepackage[figure]{hypcap}

% the pages of the TOC is numbered roman
% and a pdf-bookmark for the TOC is added
\let\myTOC\tableofcontents
\renewcommand\tableofcontents{%
  \frontmatter
  \pdfbookmark[1]{\contentsname}{}
  \myTOC
  \mainmatter }

% makes caption labels bold
% for more info about these settings, see
% http://mirrors.ctan.org/macros/latex/contrib/koma-script/doc/scrguien.pdf
\setkomafont{captionlabel}{\bfseries}
\setcapindent{1em}

% enables calculations
\usepackage{calc}

% fancy page header/footer settings
% for more information see section 9 of
% ftp://www.ctan.org/pub/tex-archive/macros/latex2e/contrib/fancyhdr/fancyhdr.pdf
\renewcommand{\chaptermark}[1]{\markboth{#1}{#1}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}

% increases the bottom float placement fraction
\renewcommand{\bottomfraction}{0.5}

% avoids that floats are placed above its sections
\let\mySection\section\renewcommand{\section}{\suppressfloats[t]\mySection}
\end_preamble
\options intoc,bibliography=totoc,index=totoc,BCOR10mm,captions=tableheading,titlepage,fleqn
\use_default_options true
\master thesis.lyx
\begin_modules
customHeadersFooters
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\float_placement h
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Your title"
\pdf_author "Your name"
\pdf_bookmarks true
\pdf_bookmarksnumbered true
\pdf_bookmarksopen true
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\pdf_quoted_options "pdfpagelayout=OneColumn, pdfnewwindow=true, pdfstartview=XYZ, plainpages=false"
\papersize a4paper
\use_geometry false
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\branch NoChildDocument
\selected 0
\filename_suffix 0
\color #ff0000
\end_branch
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 2
\paragraph_separation skip
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 2
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Left Header
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
chaptername
\end_layout

\end_inset


\begin_inset space ~
\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thechapter
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
rightmark
\end_layout

\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Enable page headers and add the chapter to the header line.
\end_layout

\end_inset


\end_layout

\begin_layout Right Header
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
leftmark
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Left Footer
\begin_inset Argument 1
status open

\begin_layout Plain Layout
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Center Footer

\end_layout

\begin_layout Right Footer
\begin_inset Argument 1
status open

\begin_layout Plain Layout

\end_layout

\end_inset


\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
thepage
\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Conclusión y futuros desarrollos
\begin_inset CommandInset label
LatexCommand label
name "chap:Conclusiones-y-futuros"

\end_inset


\end_layout

\begin_layout Standard
En este capítulo se presentan las conclusiones del estado actual del proyecto,
 así como sus futuros desarrollos y líneas de investigación a bajo nivel,
 a alto nivel, y en su conjunto.
\end_layout

\begin_layout Section
Conclusión
\begin_inset CommandInset label
LatexCommand label
name "sec:Conclusión"

\end_inset


\end_layout

\begin_layout Standard
Del alcance planteado para el trabajo a realizar se han llevado a cabo con
 éxito las siguientes mejoras propuestas:
\end_layout

\begin_layout Itemize
Generalización del algortimo para 
\emph on
k
\emph default
 dimensiones.
\end_layout

\begin_layout Itemize
Implementación de búsqueda de los 
\emph on
k
\emph default
 vecinos.
\end_layout

\begin_layout Itemize
Implementación de árboles de búsqueda binarios para la resolución del problema
 a bajo nivel.
\end_layout

\begin_layout Itemize
Determinación experimental de la influencia de los tipos de distribuciones,
 el tamaño de las celdas escogidas y el tipo de mecanismo de búsqueda empleado
 en el rendimiento del algoritmo.
\end_layout

\begin_layout Standard
Además, respecto de los resultados obtenidos en Santy
\begin_inset CommandInset citation
LatexCommand cite
key "Santy"

\end_inset

 para la búsqueda de radio fijo, aunque las mediciones se han realizado
 en procesadores distintos, su capacidad de procesamiento es similar por
 lo se puede afirmar que se mejora la escalabilidad del problema como se
 aprecia en 
\begin_inset CommandInset ref
LatexCommand ref
reference "tab:Comparativa-frente-a"

\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Comparativa frente a Santy
\begin_inset CommandInset label
LatexCommand label
name "tab:Comparativa-frente-a"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="4">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="decimal" decimal_point="." valignment="top">
<column alignment="decimal" decimal_point="." valignment="top">
<column alignment="decimal" decimal_point="." valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Iris
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\emph on
r 
\emph default
= 0.10
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\emph on
r 
\emph default
= 0.15
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\emph on
r 
\emph default
= 0.20
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Santy
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
84.8
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
199.9
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
379.6
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
R2BT
\begin_inset Foot
status open

\begin_layout Plain Layout
Nombre tentativo que recibe la implementación expuesta en esta memoria
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
92.286
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
163.898
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
269.170
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Plain Layout
\noindent
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="4">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="decimal" decimal_point="." valignment="top">
<column alignment="decimal" decimal_point="." valignment="top">
<column alignment="decimal" decimal_point="." valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Gear
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\emph on
r 
\emph default
= 0.05
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\emph on
r 
\emph default
= 0.10
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\emph on
r 
\emph default
= 0.15
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Santy
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
101.3
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
372.8
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1057.2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
R2BT
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
174.240
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
205.538
\end_layout

\end_inset
</cell>
<cell alignment="decimal" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
539.067
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage clearpage
\end_inset


\end_layout

\begin_layout Standard
Por otro lado, del 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Benchmarking-(o-comparativas)"

\end_inset

 de comparativas se pueden extraer una serie de conclusiones:
\end_layout

\begin_layout Itemize
El rendimiento de la implementación no se ve influido de forma significativa
 por diferentes tipos de distribuciones de datos reales, no presentando
 casos límite en los que el comportamiento de la implementación se degrade.
\end_layout

\begin_layout Itemize
Salvo para cantidades de puntos 
\emph on
searching
\emph default
 dos o más órdenes de magnitud menores que puntos 
\emph on
querying
\emph default
, la construcción de una estructura tipo árbol siempre está amortizada.
\end_layout

\begin_layout Itemize
Para distribuciones de datos reales y especialmente, si se va realizar una
 única búsqueda de 
\emph on
queryings
\emph default
 con una distribución diferente en cada iteración, es preferible el empleo
 de árboles 
\emph on
Stree
\emph default
 respecto a 
\emph on
Utree
\emph default
 ya que los tiempos de construcción del primero son menores y ofrecen prácticame
nte el mismo rendimiento.
\end_layout

\begin_layout Itemize
Es preferible, a alto nivel, que el tamaño de las celdas, o lo que es lo
 mismo, el número de estas, se escoja de forma que cada celda agrupe en
 promedio una cantidad de puntos similar al tamaño de los resultados deseados,
 obteniéndose para este caso los mejores rendimientos.
\end_layout

\begin_layout Itemize
Es preferible usar, a bajo nivel, las celdas explicadas en este trabajo
 para la búsqueda de radio fijo.
\end_layout

\begin_layout Itemize
Es preferible usar, a bajo nivel, una única celda de ANN para la búsqueda
 de los 
\emph on
k
\emph default
 vecinos.
\end_layout

\begin_layout Standard
No se debe olvidar el propósito último del trabajo, que es proporcionar
 una librería funcional, fácilmente implementable y a la par que otras existente
s para su aplicación en métodos numéricos.
 En ese sentido, se cumplen las siguientes características:
\end_layout

\begin_layout Itemize
No existen dependencias externas respecto a librerías no pertenecientes
 a la librería estándar de C
\family typewriter
++
\family default
.
\end_layout

\begin_layout Itemize
El usuario no necesita gestionar la reserva y destrucción de memoria dinámica.
\end_layout

\begin_layout Itemize
La interfaz permite al usuario trabajar con su propia definición de puntos,
 y almacenados en cualquier contenedor de la librería estándar de C
\family typewriter
++
\family default
.
\end_layout

\begin_layout Itemize
Se emplea un C
\family typewriter
++
\family default
 moderno, ahorrando al usuario el empleo de sintaxis cercana a C más propensa
 a fugas de memoria.
\end_layout

\begin_layout Itemize
Se ofrecen rendimientos mejores o del mismo orden de magnitud que otras
 implementaciones existentes.
\end_layout

\begin_layout Standard
El desarrollo de aplicaciones orientadas a mejorar el rendimiento de soluciones
 existentes para problemas reales supone una mejora sustancial en cualquier
 proceso productivo.
 En este caso, dada la generalidad de los elementos finitos, cualquier mejora
 sustancial en las herramientas que permiten su empleo se traduce en la
 inmediata mejora de los cálculos en el mundo de la ingeniería.
 Esto incluye, pero no se limita a numerosas áres del conocimiento como
 la transferencia de calor, la mecánica de fluidos, la resistencia de materiales
 o el electromagnetismo.
 Lo que permite la fabricación de componentes más baratos y eficientes,
 redundando en un ahorro energético y de materias primas.
\end_layout

\begin_layout Section
Líneas de investigación
\end_layout

\begin_layout Standard
La solución propuesta puede encontrarse en el repositorio público 
\begin_inset Flex URL
status open

\begin_layout Plain Layout

https://bitbucket.org/stapia/r2bt
\end_layout

\end_inset

 .
 Aunque funcional, la solución aún es un prototipo, pues existen numerosas
 optimizaciones y líneas de desarrollo para mejorar su rendimiento.
\end_layout

\begin_layout Subsection
Bajo nivel
\end_layout

\begin_layout Paragraph
Búsqueda del mejor vecino
\end_layout

\begin_layout Standard
La primera mejora que se puede incluir al trabajo es dotar a las celdas
 de un método de búsqueda especializado para la búsqueda del mejor vecino,
 en vez de usar el método de búsqueda de los 
\emph on
k
\emph default
 vecinos particularizado con 
\emph on
k=1
\emph default
.
 Esto es así porque para un único vecino sería posible ahorrar comparaciones
 y bucles usados en el algoritmo de búsqueda de los 
\emph on
k
\emph default
 vecinos, siendo esto cierto para todos los tipos de celda.
\end_layout

\begin_layout Paragraph*
Inserción / Extracción de puntos en árboles
\end_layout

\begin_layout Standard
Otra mejora posible es explorar la posibilidad de insertar o extraer puntos
 de los 
\emph on
kd-Trees.

\emph default
 De esta forma se iría construyendo el árbol a medida que se insertan puntos
 en la celda, no teniendo que esperar a que estén todos los puntos insertados
 para poder empezar a construir el árbol.
 Además, no sería necesario el uso de algoritmos de ordenación para su construcc
ión.
 Habría que comprobar, sin embargo, que este método ofrece mejoras en el
 rendimiento respecto al empleo de algoritmos de ordenación.
 De realizarse la implementación de esta mejora con éxito, se podría hablar
 de 
\emph on
self-balancing-trees, 
\emph default
es decir, de árboles de búsqueda binarios que se mantienen equilibrados
 en todo momento.
\end_layout

\begin_layout Paragraph*
Optimización de algoritmos de ordenación
\end_layout

\begin_layout Standard
Las implementaciones de los algoritmos empleados para buscar la mediana
 (
\emph on
quickselect
\emph default
) y para realizar las particiones (
\emph on
dutch national flag problem
\emph default
), son implementaciones básicas en el sentido de que se aplican ciegamente
 a todos los casos y las optimizaciones con las que cuentan no son sofisticadas.
 Existen en la literatura numerosos algoritmos más óptimos que los empleados
 aunque de una implementación más laboriosa.
 Además, se intuye que el algoritmo de partición no es del todo óptimo ya
 que realiza una ordenación innecesaria.
 Esto es, no es necesario que todos los puntos con coordenada igual a la
 mediana se encuentren agrupados en bloque justo antes de la mediana desplazada
 y a la derecha del bloque de puntos menores que la mediana.
 Es por ello que la búsqueda (o desarrollo) de un algoritmo de partición
 que gestione valores repetidos sin ordenarlos de forma especial podría
 suponer una mejora significativa en la velocidad de construcción.
 Destacamos los siguientes algoritmos:
\end_layout

\begin_layout Itemize
Algoritmo Floyd–Rivest, para buscar la mediana 
\begin_inset CommandInset citation
LatexCommand cite
key "AlgrthmFLoyd"

\end_inset

, sustituyendo a 
\emph on
quick
\family typewriter
_
\family default
select
\emph default
.
\end_layout

\begin_layout Itemize
Algoritmo de partición de Hoare, para realizar la partición
\begin_inset Foot
status open

\begin_layout Plain Layout
La implementación de este algoritmo no es especialmente compleja, pero a
 diferencia de 
\emph on
dutch national flag problem
\emph default
, no hay forma trivial de obtener el índice de la mediana desplazada 
\end_layout

\end_inset

 
\begin_inset CommandInset citation
LatexCommand cite
key "AlgrthmHoare"

\end_inset

.
\end_layout

\begin_layout Paragraph*
Paso de recursividad a iteración
\end_layout

\begin_layout Standard
Para árboles con un gran cantidad de puntos es posible que haya que sumergirse
 en muchos niveles de recursividad, llenando la pila de llamadas previas
 a funciones de niveles superiores.
 Para evitar esto, quizás sea interesante usar algún método iterativo empleando
 colas o algún otro tipo de estructura de datos que no altere la funcionalidad
 del algoritmo pero sí que haga un uso más eficiente de la memoria.
 Habría que comprobar que tal implementación es posible, y que esta produce
 mejores rendimientos que la implementación recursiva actual.
\end_layout

\begin_layout Subsection
Alto nivel
\end_layout

\begin_layout Paragraph*
Empleo de contenedores genéricos
\end_layout

\begin_layout Standard
En las implementaciones existentes de celdas, y a la hora de trasladar resultado
s entre las distintas partes de la implementación se emplea el uso de vectores.
 Estos, que ocupan un espacio de memoria contiguo y no son la mejor estructura
 de datos a la hora de insertar elementos al principio o a mitad de los
 mismos, podrían ser sustituidos por otro tipo de estructuras de datos como
 listas o colas que puedan ofrecer mejores rendimientos.
\end_layout

\begin_layout Paragraph*
Elección dinámica del algoritmo a bajo nivel
\end_layout

\begin_layout Standard
En función de los resultados obtenidos en el 
\emph on
benchmarking
\emph default
, y otros que puedan ser obtenidos a partir de nuevas distribuciones de
 datos, se puede inducir en qué casos cada tipo de celda de bajo nivel presenta
 mejores rendimientos.
 Se podría entonces optimizar el uso de los distintos tipos de celda dependiendo
 del tipo de distribución con el que se trabaje.
\end_layout

\begin_layout Paragraph*

\emph on
Machine learning
\end_layout

\begin_layout Standard
En la misma línea que en el apartado anterior, en vez de ponderar y considerar
 manualmente los resultados obtenidos, podría ser posible elaborar un sistema
 de retroalimentación en el que se alimente al algoritmo de múltiples distribuci
ones de datos.
 De esta forma, se podría diseñar un sistema que solucione el problema de
 la vecindad de puntos para cada distribución empleando a bajo nivel, diversas
 combinaciones de subtipos de celdas.
 Así, el propio sistema podría modificar sus parámetros para favorecer aquellas
 celdas que presenten rendimientos mejores sobre otras ante distintos tipos
 de distribuciones.
 Se tendría entonces un sistema de aprendizaje autónomo que optimizaría
 el algoritmo tras suficientes análisis de conjuntos de datos.
\end_layout

\begin_layout Paragraph*
Distancia definida por el usuario
\end_layout

\begin_layout Standard
Se podría por otro lado, fomentar la generalidad del algoritmo.
 En vez de limitarlo al trabajo en el espacio con distintas euclídeas, se
 podría, emulando las funciones de 
\emph on
templates 
\emph default
de la librería estándar de C
\family typewriter
++
\family default
, crear una librería 
\begin_inset Quotes eld
\end_inset

plantilla
\begin_inset Quotes erd
\end_inset

 que cada usuario final podría adaptar a sus necesidades.
\end_layout

\begin_layout Paragraph*
Gestión de espacios multidimensionales
\end_layout

\begin_layout Standard
Si bien se ha realizado un estudio teórico de la viabilidad del algoritmo
 para espacios multidimensionales, afirmando que sería posible adaptar el
 algoritmo para que en este tipo de espacios se limite a emplear métodos
 de fuerza bruta, o construir 
\emph on
kd-Trees 
\emph default
sin emplear el algoritmo de alto nivel, la implementación no se llega a
 realizar.
 Se podría determinar experimentalmente a partir de qué dimensión las búsquedas
 se empiezan a degradar, y gestionar las búsquedas por los métodos alternativos
 mencionados anteriormente.
 Cabe decir que esta tarea podría ser también asimilable dentro de un sistema
 de aprendizaje autónomo.
\end_layout

\begin_layout Paragraph*
Paralelización empleando GPU
\end_layout

\begin_layout Standard
Se sabe que los fabricantes de unidades de procesamiento gráfico (GPU en
 inglés) han avazando en los últimos años en la mejora de librerías que
 permiten la ejecución de procesos paralela en estas unidades.
 Este avance es muy prometedor respecto a la posibilidad de paralelizar
 el proceso no ya en las decenas de núcleos CPU, si no en los cientos de
 núcleos de la GPU, lo que supondría una gran mejora en el rendimiento.
\end_layout

\begin_layout Standard
\begin_inset Branch NoChildDocument
status collapsed

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "thesisExample"
options "alpha"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset nomencl_print
LatexCommand printnomenclature
set_width "custom"
width "2.5cm"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
