/****************************************************************************
 * Basic node (for task 3)
*****************************************************************************/

#ifndef _BASIC_NODE_
#define _BASIC_NODE_

#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>


/* Basic node */
template <class T, int dim>
struct node {

	T data;
	double v[dim];

	struct node *left, *right;

	/* Constructors */
	node(): data(0), left(NULL), right (NULL) {
		for(int i = 0; i < dim; i++){
			v[i]=0;
		}
	}

	node(const double *w): data(0), left(NULL), right (NULL) {
		memcpy(v, w, dim*sizeof(double));
	}

	/* Functions */
	void show_tree(int depth = 0){
		if(!depth) {
			std::cout << "Root: ";
		}
		std::cout << data << std::endl;
		//show(this);
		if(left) {
			for(int i = 0; i < 1 + depth; i++){
			std::cout << "\t";
			}
			std::cout << "Left: ";
			left->show_tree(depth+1);
		}
		if(right) {
			for(int i = 0; i < 1 + depth; i++){
			std::cout << "\t";
			}
			std::cout << "Right: ";
			right->show_tree(depth+1);
		}
		return;
	}

	/* Functions */
	void show_info_tree(int depth = 0){
		if(!depth) {
			std::cout << "Root: " ;
		}
		std::cout << depth%dim << "-> " << this->v[depth%dim] << " // "; //<< std::endl;
		show(this);
		if(left) {
			for(int i = 0; i < 1 + depth; i++){
			std::cout << "\t";
			}
			std::cout << "Left: ";
			left->show_info_tree(depth+1);
		}
		if(right) {
			for(int i = 0; i < 1 + depth; i++){
			std::cout << "\t";
			}
			std::cout << "Right: ";
			right->show_info_tree(depth+1);
		}
		return;
	}
};


/* Loads nodes from file to vector of nodes */
template <class T, int dim>
void load_nodes(std::vector<node<T,dim>>& points, const char* filename, int num = 0) {
	int index = 0;
	int coord = 0;
	node<T,dim> point;
	std::ifstream input(filename);
	if (input.is_open()){
		while ( input.good() && index<num ){
			input>>point.v[coord];
			coord++;
			if ( coord == dim ){
				coord = 0;
				index++;
				point.data = index;
				points.push_back(point);
			}
		}
	std::cerr << "File " << filename << " successfully read. Loaded " << index <<" nodes.\n";
	}
	else{
		std::cerr<<"Problem reading file\n";
	}
}

/* For std::sort */
template <class T, int dim>
bool node_sorter(const node<T,dim>* x, const node<T,dim>* y){
	return (x->data < y->data);
}

/* Returns the quadratic distance */
template <class T, int dim>
inline double dist(node<T,dim>& a, node<T,dim>& b) {
	double t, d = 0;
	int coord = dim;
	while (coord--) {
		t = a.v[coord] - b.v[coord];
		d += t * t;
	}
	return d;
}

template <class T, int dim>
inline double dist(node<T,dim>* a, node<T,dim>* b) {
	double t, d = 0;
	int coord = dim;
	while (coord--) {
		t = a->v[coord] - b->v[coord];
		d += t * t;
	}
	return d;
}

/* Shows the node through standard output */
template <class T, int dim>
void show(node<T,dim>& a) {
	std::cout << a.data << ": ";
	for(int i = 0; i < dim; i++){
	std::cout << a.v[i] << " ";
	}
	std::cout << "\n";
}

template <class T, int dim>
void show(node<T,dim>* a) {
	std::cout << a->data << ": ";
	for(int i = 0; i < dim; i++){
	std::cout << a->v[i] << " ";
	}
	std::cout << "\n";
}

/* Shows a vector of nodes through standard output */
template <class T, int dim>
void show(std::vector<node<T,dim>>& points) {
	int num = points.size();
	for(int i = 0; i < num; i++){
		show(points[i]);
	}
}

/* Shows the node and its distance to a query point through standard output */
template <class T, int dim>
void showdist(node<T,dim>& a, node<T,dim>& b) {
	std::cout << "\t" << a.data << ": ";
	for(int i = 0; i < dim; i++){
	std::cout<< a.v[i] << " ";
	}
	std::cout<< "\tDist: " << dist(a, b);
	std::cout<< "\n";
}

template <class T, int dim>
void showdist(node<T,dim>* a, node<T,dim>* b) {
	std::cout << "\t" << a->data << ": ";
	for(int i = 0; i < dim; i++){
	std::cout<< a->v[i] << " ";
	}
	std::cout<< "\tDist: " << dist(*a, *b);
	std::cout<< "\n";
}

/* Shows a vector of nodes and its distance to a query point through standard output */
template <class T, int dim>
void showdist(std::vector<node<T,dim>>& points, node<T,dim>& query) {
	int num = points.size();
	for(int i = 0; i < num; i++){
		showdist(points[i], query);
	}
}

template <class T, int dim>
void showdist(std::vector<node<T,dim>*>& points, node<T,dim>& query) {
	int num = points.size();
	for(int i = 0; i < num; i++){
		showdist(*points[i], query);
	}
}


#endif
