#include <chrono>

#include "Basic_node.hpp" 		/* For node */
#include "Basic_bench.hpp"
#include "Rosetta_tree.hpp"     /* For tree */
#include "Rosetta_bench.hpp"
#include "xmlReader.hpp"

#define DIM 1
#define READ 9

int main(){
	std::vector<node<int, DIM>> the_points;
	node<int, DIM>* median;
	
	load_nodes(the_points, "prueba.txt", READ);	
	
	std::cout <<"\nPRIMER TEST: \n";
	
	std::cout <<"The points: \n";
	show(the_points);
	median = find_median(&the_points[0], &the_points[READ-1], 0);
	std::cout <<"The median is : \n";
	show(median);
	std::cout <<"The points: \n";
	show(the_points);

	
	std::cout <<"\nSEGUNDO TEST: \n";
	node<int, DIM>* root;
	
	root = make_tree(&the_points[0], READ, 0);
	root->show_info_tree();
	
	return 0;
}

