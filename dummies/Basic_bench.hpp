/****************************************************************************
 *Basic test bench (for task 3)
*****************************************************************************/

#ifndef _BASIC_BENCH_
#define _BASIC_BENCH_

#include "Basic_node.hpp" 		/* For node */


/* Returns the pointer to the closest node to the query */
template <class T, int dim>
struct node<T,dim>* nearest_bruteforce(std::vector<node<T,dim>>& points, node<T,dim>& query){

	double distance;
	double best_distance = dist(query, points[0]);
	int num = points.size();
	node<T,dim>* nearest_node;
	for(int j = 0; j < num; j++){
		distance = dist(query, points[j]);
		if(distance < best_distance){
			best_distance = distance;
			nearest_node = &points[j];
		}
	}
	return nearest_node;
}

/* Returns a pointer to a vector with the n closest nodes to the query */
template <class T, int dim>
std::vector<node<T,dim>*> neighbors_bruteforce(std::vector<node<T,dim>>& points, node<T,dim>& query, int n = 2){

	double distance;
	double max_distance = 0;
	int max_index = 0;
	int num = points.size();
	std::vector <node<T,dim>*> neighbors_node;
	neighbors_node.reserve(n);
	int i = 0;
	for(int j = 0; j < num; j++){
		distance = dist(points[j], query);
		if(i < n){ /* Fill neighbor vector */
			neighbors_node.push_back(&points[j]);
			if(distance > max_distance){
				max_distance = distance;
				max_index = i;
			}
			i++;
		}
		else if(distance < max_distance){
			neighbors_node[max_index] = &points[j];
			max_distance = 0;
			for(int k = 0; k < n; k++){ /*Tag the biggest distance*/
				double aux_dist = dist(query, *neighbors_node[k]);
				if( aux_dist > max_distance){
					max_distance = aux_dist;
					max_index = k;
				}
			}
		}
	}
	return neighbors_node;
}

/* Returns a pointer to a list of all the nodes  within the radius r of the query */
template <class T, int dim>
std::vector <node<T,dim>*> radius_bruteforce(std::vector<node<T,dim>>& points, node<T,dim>& query, const double r){

	double distance;
	const double qradius = r*r;
	int num = points.size();
	std::vector <node<T,dim>*> neighbors_node;
	for(int j = 0; j < num; j++){
		distance=dist(points[j], query);
		if(distance < qradius){
			neighbors_node.push_back(&points[j]);
		}
	}
	return neighbors_node;
}

template <class T, int dim>
int neighbors_key (std::vector<node<T,dim>*>& neighbors){
	int count = 0;
	int n = neighbors.size();
	for(int i = 0; i < n; i++){
		count+=neighbors[i]->data;
	}
	return count;
}


#endif
