#include <chrono>
#include <algorithm>
#include <fstream>
#include <iomanip>

#include "Basic_node.hpp" 		/* For node */
#include "Basic_bench.hpp"
#include "Rosetta_tree.hpp"     /* For tree */
#include "Rosetta_bench.hpp"
#include "Launch_options.hpp"
#include "xmlReader.hpp"

#define DIM 3 /* We need to define DIM for Julius_bench */

#define LOOPS 10

#include "Julius_bench.hpp"		/* For julius */


//#define _PRINTING_
#define _OUTPUT_

using namespace KDTree;
using namespace std::chrono;

launch_options options;

int main(int argc, char **argv){

	options.load(argc, argv);
	/* Loading options */
	int data_number = options.data_number;
	int query_number = options.query_number;
	int neigh_number = options.neighbor_number;
	const double radius = options.radius;

	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
	double timings[11] = {0};

	std::vector<node<int, DIM>> the_points;
	std::vector<node<int, DIM>> the_querys;

	/*
	load_nodes(the_points, options.data_file, data_number);
	load_nodes(the_querys, options.query_file, query_number);
	*/
	
	point_loader(the_points, options.data_file);
	point_loader(the_querys, options.query_file);
	data_number = the_points.size();
	query_number = the_querys.size();
	

	#ifdef _PRINTING_
			show(the_points);
			std::cout <<"The points: \n";
			std::cout <<"The querys: \n";
			show(the_querys);
	#endif // _PRINTING_
	
	node<int, DIM>* root;

	std::list<Item<int, DIM> > items = load_julius(the_points);
	std::list<Item<int, DIM> > julius_querys = load_julius(the_querys);
	std::list<Item<int, DIM> >::iterator querysIt = julius_querys.begin();

	int n_errors = 0, s_errors = 0, j_errors = 0, jn_errors = 0;

	for(int i = 0; i < query_number; i++){

		#ifdef _PRINTING_
			node<int, DIM>* nearest = nearest_bruteforce(the_points, the_querys[i]);
			std::cout <<"\nBRUTE FORCE: \n";
			std::cout <<"The nearest: \n";
			showdist(nearest, &the_querys[i]);
		#endif // _PRINTING_
		t1 = std::chrono::high_resolution_clock::now();
		std::vector<node<int, DIM>*> the_neighbors = neighbors_bruteforce(the_points, the_querys[i], neigh_number);
		t2 = std::chrono::high_resolution_clock::now();
		timings[0] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		int nbf_key = neighbors_key(the_neighbors);

		#ifdef _PRINTING_
			std::cout <<"The neighbours: \n";
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			showdist(the_neighbors, the_querys[i]);
			std::cout <<"The sphere: \n";
		#endif // _PRINTING_

		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = radius_bruteforce(the_points, the_querys[i], radius);
		t2 = std::chrono::high_resolution_clock::now();
		timings[1] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		int sbf_key = neighbors_key(the_neighbors);

		#ifdef _PRINTING_
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			showdist(the_neighbors, the_querys[i]);
		#endif // _PRINTING_

		if(i<LOOPS){
			t1 = std::chrono::high_resolution_clock::now();
			root = make_tree(&the_points[0], data_number, 0);
			t2 = std::chrono::high_resolution_clock::now();
			timings[2] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		}

		#ifdef _PRINTING_
			nearest = nearest_rosetta(root, &the_querys[i]);
			std::cout <<"\nROSETTA: \n";
			std::cout <<"Tree built: \n";
			root->show_tree();
			std::cout <<"The nearest: \n";
			showdist(nearest, &the_querys[i]);
		#endif // _PRINTING_

		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = neighbors_rosetta(root, &the_querys[i], neigh_number);
		t2 = std::chrono::high_resolution_clock::now();
		timings[3] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		int nros_key = neighbors_key(the_neighbors);

		#ifdef _PRINTING_
			std::cout <<"The neighbours: \n";
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			showdist(the_neighbors, the_querys[i]);
			std::cout <<"The sphere: \n";
		#endif // _PRINTING_

		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = radius_rosetta(root, &the_querys[i], radius);
		t2 = std::chrono::high_resolution_clock::now();
		timings[4] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		int sros_key = neighbors_key(the_neighbors);

		#ifdef _PRINTING_
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			showdist(the_neighbors, the_querys[i]);
		#endif // _PRINTING_

		if(options.julius_tests){
			SimpleTreeBuilder<int, DIM> simple_builder;
			MidpointTreeBuilder<int, DIM> midpoint_builder;
			t1 = std::chrono::high_resolution_clock::now();
			PointSplitNode<int, DIM>* simple_root = simple_builder.build(items);
			t2 = std::chrono::high_resolution_clock::now();
			timings[5] += (double) duration_cast<microseconds>( t2 - t1 ).count();
			t1 = std::chrono::high_resolution_clock::now();
			PointSplitNode<int, DIM>* midpoint_root = midpoint_builder.build(items);
			t2 = std::chrono::high_resolution_clock::now();
			timings[6] += (double) duration_cast<microseconds>( t2 - t1 ).count();
			t1 = std::chrono::high_resolution_clock::now();
			PlaneSplitNode<int, DIM>* simple_plane_split_root;
			simple_plane_split_root = simple_builder.build_plane_split(items, the_points.size());
			t2 = std::chrono::high_resolution_clock::now();
			timings[7] += (double) duration_cast<microseconds>( t2 - t1 ).count();

			int js_key = 0;
			int jmp_key = 0;
			int jsps_key = 0;
			t1 = std::chrono::high_resolution_clock::now();
			std::list<int> js_neighbors_list = neighbors_julius(simple_root, *querysIt, neigh_number, js_key);
			t2 = std::chrono::high_resolution_clock::now();
			timings[8] += (double) duration_cast<microseconds>( t2 - t1 ).count();
			t1 = std::chrono::high_resolution_clock::now();
			std::list<int> jmp_neighbors_list = neighbors_julius(midpoint_root, *querysIt, neigh_number, jmp_key);
			t2 = std::chrono::high_resolution_clock::now();
			timings[9] += (double) duration_cast<microseconds>( t2 - t1 ).count();
			t1 = std::chrono::high_resolution_clock::now();
			std::list<int> jsps_neighbors_list = neighbors_julius(simple_plane_split_root, *querysIt, neigh_number, jsps_key);
			t2 = std::chrono::high_resolution_clock::now();
			timings[10] += (double) duration_cast<microseconds>( t2 - t1 ).count();

			#ifdef _PRINTING_
				std::cout <<"\nJULIUS: \n";
				std::cout <<"The neighbours with: \n";
				std::cout <<"1. Simple tree: \n";
				print_julius(js_neighbors_list);
				std::cout <<"2. Midpoint tree: \n";
				print_julius(jmp_neighbors_list);
				std::cout <<"3. Plane split tree: \n";
				print_julius(jsps_neighbors_list);
				if(js_key != jmp_key || js_key != jsps_key || jmp_key != jsps_key) std::cerr << "Julis don't match between them\n";
				if(js_key != nbf_key) std::cerr << "Julis don't match with bruteforce\n";
			#endif // _PRINTING_
			if(js_key != jmp_key || js_key != jsps_key || jmp_key != jsps_key) j_errors++;
            if(js_key != nbf_key) jn_errors++;
		} //Julius_tests

		if(nbf_key != nros_key) n_errors++;
		if(sbf_key != sros_key) s_errors++;
		querysIt++;

		#ifdef _PRINTING_
		if(nbf_key != nros_key) std::cerr << "Neighbours don't match\n";
		if(sbf_key != sros_key) std::cerr << "Spheres don't match\n";
		#endif // _PRINTING_

	} // End for

	#ifdef _OUTPUT_
		std::ofstream out("timers.log",std::ofstream::app);
		out << std::fixed;
		out  << "Data number: " << data_number << "\n"
			 << "Query number: " << query_number << "\n"
			 << "Neighbor number: " << neigh_number << "\n"
			 << "Radius: " << radius << "\n"
			 << "Neighbors w/ bruteforce: " << timings[0]/query_number << " us\n"
			 << "Sphere w/ bruteforce: " << timings[1]/query_number << " us\n"
			 << "Kdtree making: " << timings[2]/LOOPS << " us\n"
			 << "Neighbors w/ rosetta: " << timings[3]/query_number << " us\n"
			 << "Sphere w/ rosetta: " << timings[4]/query_number << " us\n";
			if(options.julius_tests){
			out	 << "Julis tree making: (simple/midpoint/planesplit)\n"
				 << timings[5]/query_number << "/" << timings[6]/query_number << "/" << timings[7]/query_number << " us\n"
				 << "Julius search times: (simple/midpoint/planesplit)\n"
				 << timings[8]/query_number << "/" << timings[9]/query_number << "/" << timings[10]/query_number << " us\n"
				 << "Julius neighbors list didn't match between them in " << j_errors << " cases\n"
				 << "Julius simple tree neighbors didn't match w/ bruteforce in " << jn_errors << " cases\n";
			}
		out  << "Neighbours didn't match in " << n_errors << " cases\n"
			 << "Spheres didn't match in " << s_errors << " cases\n";
		out << "_______________________________________________________\n";
		out.close();
	#endif // _OUTPUT_


	return 0;
}
