#include <chrono>
#include <algorithm>
#include <fstream>
#include <iomanip>

#include "Basic_node.hpp" 		/* For node */
#include "Basic_bench.hpp"
#include "Rosetta_tree.hpp"     /* For tree */
#include "Rosetta_bench.hpp"
#include "Launch_options.hpp"
#include "xmlReader.hpp"

#define DIM 3

#define LOOPS 10

using namespace std::chrono;

launch_options options;

int main(int argc, char **argv){

	options.load(argc, argv);
	/* Loading options */
	int printing = options.printing;
	int output = options.output;

	int data_number = options.data_number;
	int query_number = options.query_number;
	int neigh_number = options.neighbor_number;
	const double radius = options.radius;

	std::chrono::high_resolution_clock::time_point t1;
	std::chrono::high_resolution_clock::time_point t2;
	double timings[5] = {0};

	std::vector<node<int, DIM>> the_points;
	std::vector<node<int, DIM>> the_querys;

	node<int, DIM>* root;
	std::vector<node<int, DIM>*> the_neighbors;

	/*
	load_nodes(the_points, options.data_file, data_number);
	load_nodes(the_querys, options.query_file, query_number);
	*/

	point_loader(the_points, options.data_file);
	point_loader(the_querys, options.query_file);

	if(!data_number) data_number = the_points.size();
	else the_points.resize(data_number);

	if(!query_number) query_number = the_querys.size();
	else the_querys.resize(query_number);

	int one_errors = 0, n_errors = 0, s_errors = 0;

	int nbf_keys[query_number];
	int sbf_keys[query_number];
	int ntree_keys[query_number];
	int stree_keys[query_number];

	/*
	if(printing){
			std::cout <<"The points: \n";
			show(the_points);
			std::cout <<"The querys: \n";
			show(the_querys);

	}
	*/

	std::cout <<"Number of points set to: " << data_number << "\n";
	std::cout <<"Number of querys set to: " << query_number << "\n";
	std::cout <<"Number of neighbours set to: " << neigh_number << "\n";
	std::cout <<"Radius set to: " << radius << " (" << radius*radius << ")\n\n";

	std::cout << "BRUTEFORCE\n";

	std::cout << "Doing nearest neighbour search for " << neigh_number << " neighbours\n";


	for(int i = 0; i < query_number; i++){
		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = neighbors_bruteforce(the_points, the_querys[i], neigh_number);
		t2 = std::chrono::high_resolution_clock::now();
		timings[0] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		nbf_keys[i] = neighbors_key(the_neighbors);

		if(printing){
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			show(the_querys[i]);
			showdist(the_neighbors, the_querys[i]);
		}
	}

	std::cout << "Doing neighbour search for sphere of radius " << radius << "\n";

	for(int i = 0; i < query_number; i++){
		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = radius_bruteforce(the_points, the_querys[i], radius);
		t2 = std::chrono::high_resolution_clock::now();
		timings[1] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		sbf_keys[i] = neighbors_key(the_neighbors);
		
		if(printing){
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			show(the_querys[i]);
			showdist(the_neighbors, the_querys[i]);
		}
	}

	std::cout << "KDTREE\n";

	std::cout << "Building tree\n";

	for(int i = 0; i < LOOPS; i++){
		t1 = std::chrono::high_resolution_clock::now();
		root = make_tree(&the_points[0], data_number, 0);
		t2 = std::chrono::high_resolution_clock::now();
		timings[2] += (double) duration_cast<microseconds>( t2 - t1 ).count();
	}


	//if(printing)show_tree();
		root->show_info_tree();


	std::cout << "Doing nearest neighbour search for " << neigh_number << " neighbours\n";

	for(int i = 0; i < query_number; i++){
		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = neighbors_rosetta(root, &the_querys[i], neigh_number);
		t2 = std::chrono::high_resolution_clock::now();
		timings[3] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		
		if(printing){
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			show(the_querys[i]);
			showdist(the_neighbors, the_querys[i]);
		}
	}

	std::cout << "Doing neighbour search for sphere of radius " << radius << "\n";
	for(int i = 0; i < query_number; i++){
		the_neighbors.clear();
		t1 = std::chrono::high_resolution_clock::now();
		the_neighbors = radius_rosetta(root, &the_querys[i], radius);
		t2 = std::chrono::high_resolution_clock::now();
		timings[4] += (double) duration_cast<microseconds>( t2 - t1 ).count();
		
		if(printing){
			std::sort (the_neighbors.begin(), the_neighbors.end(), node_sorter<int,DIM>);
			show(the_querys[i]);
			showdist(the_neighbors, the_querys[i]);
		}
	}

	/* Checking for mismatches */
	for(int i = 0; i < query_number; i++){
		node<int, DIM>* bf_nearest = nearest_bruteforce(the_points, the_querys[i]);
		node<int, DIM>* tree_nearest = nearest_rosetta(root, &the_querys[i]);
		if(bf_nearest != tree_nearest) one_errors++;
		if(nbf_keys[i] != ntree_keys[i]) n_errors++;
		if(sbf_keys[i] != stree_keys[i]) s_errors++;
	}

	if(printing){
		if(n_errors) std::cout << "Neighbours didn't match in " << n_errors << " cases\n";
		if(s_errors) std::cout << "Spheres didn't match in " << s_errors << " cases\n";
		if(one_errors) std::cout << "Single neighbour didn't match in " << one_errors << " cases\n";
	}

	if(output){
		std::ofstream out("timers.log",std::ofstream::app);
		out << std::fixed;
		out  << "Data number: " << data_number << "\n"
			 << "Query number: " << query_number << "\n"
			 << "Neighbor number: " << neigh_number << "\n"
			 << "Radius: " << radius << "\n"
			 << "Neighbors w/ bruteforce: " << timings[0] << " us\n"
			 << "Sphere w/ bruteforce: " << timings[1] << " us\n"
			 << "Kdtree making: " << timings[2]/LOOPS << " us\n"
			 << "Neighbors w/ rosetta: " << timings[3] << " us\n"
			 << "Sphere w/ rosetta: " << timings[4] << " us\n";
		out  << "Neighbours didn't match in " << n_errors << " cases\n"
			 << "Spheres didn't match in " << s_errors << " cases\n"
			 << "Single neighbour didn't match in " << one_errors << " cases\n";
		out << "_______________________________________________________\n";
		out.close();
	}

	return 0;
}


