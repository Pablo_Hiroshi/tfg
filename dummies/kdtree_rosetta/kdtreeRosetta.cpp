#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define MAX_DIM 3

// Cada punto tiene MAX_DIM componentes y guarda cual es su rama izqda y su rama dcha.
struct kd_node_t{
    double x[MAX_DIM];
    struct kd_node_t *left, *right;
};

 // Esta funci�n devuelve la distancia (al cuadrado) de cada punto
    inline double
dist(struct kd_node_t *a, struct kd_node_t *b, int dim)
{
    double t, d = 0;
    while (dim--) { //Cada repetici�n del bucle resta 1 a dim
        t = a->x[dim] - b->x[dim];
        d += t * t;
    }
    return d; // retorna la distancia al cuadrado
}

// Esta funci�n intercambia las componentes de dos puntos
inline void swap(struct kd_node_t *x, struct kd_node_t *y) {
    double tmp[MAX_DIM];
	//memcpy copia los bytes tal cual del 2o argumento en el 1ero, el 3er argumento indica el numero de bytes a copiar
    memcpy(tmp,  x->x, sizeof(tmp)); //recordar que x es el vector de componente de un punto
    memcpy(x->x, y->x, sizeof(tmp));
    memcpy(y->x, tmp,  sizeof(tmp));
}


// Esta funci�n devuelve la mediana
/* see quickselect method */ //https://en.wikipedia.org/wiki/Quickselect
//https://www.youtube.com/watch?v=MZaf_9IZCrc
    struct kd_node_t*
find_median(struct kd_node_t *start, struct kd_node_t *end, int idx)
{
    if (end <= start) return NULL;
    if (end == start + 1)
        return start;

    struct kd_node_t *p, *store, *md = start + (end - start) / 2; //vectores de structs
    double pivot;
    while (1) {
        pivot = md->x[idx];//copiamos la componente idx de md en la auxiliar pivot

        swap(md, end - 1); //intercambiamos los valores del pen�ltimo nodo con md
        for (store = p = start; p < end; p++) { //store=start; p=start;
            if (p->x[idx] < pivot) { //si el componente de alg�n punto es menor que la mediana
                if (p != store) //y no es igual al valor almacenado previamente
                    swap(p, store); //intercambiamos el punto con el valor almacenado
                store++; //pasamos al siguiente punto
            }
        }
        swap(store, end - 1); //intercambiamos el punto de store con el penultimo punto

        /* median has duplicate values */
        if (store->x[idx] == md->x[idx]) //si el valor almacenado es el mismo que la mediana retornamos mediana
            return md;

        if (store > md) end = store; //si el valor almacenado es mayor que la mediana el �ltimo punto ser� el almacenado
        else        start = store; //si es menor, el primer punto ser� el almacenado
    }
}

    struct kd_node_t*
make_tree(struct kd_node_t *t, int len, int i, int dim)
{
    struct kd_node_t *n;

    if (!len) return 0; // ! operador not

    if ((n = find_median(t, t + len, i))) { //Igualamos a n, el puntero que apunta a la mediana
        i = (i + 1) % dim; //1 -> 2 -> 3 vamos rotando en cada nivel del arbol la coordenada respecto a la que ordenamos
		//Recursividad
        n->left  = make_tree(t, n - t, i, dim); //Guardamos en cada punto la rama a la que apunta por la izqda
        n->right = make_tree(n + 1, t + len - (n + 1), i, dim); //Y por la derecha
    }
    return n;
}

/* global variable, so sue me */
int visited;

void nearest(struct kd_node_t *root, struct kd_node_t *nd, int i, int dim,
        struct kd_node_t **best, double *best_dist)
{
    double d, dx, dx2;

    if (!root) return; // si no estamos en la raiz, nos salimos (Hay recursividad, va descendiendo)
    d = dist(root, nd, dim); // distancia de la raiz a nd
    dx = root->x[i] - nd->x[i];
    dx2 = dx * dx;

    visited ++;

    if (!*best || d < *best_dist) { //Si no hay mejor punto o la nueva distancia mejora la ultima mejor
        *best_dist = d; //nueva mejor distancia
        *best = root; //nuevo mejor punto
    }

    /* if chance of exact match is high */
    if (!*best_dist) return; //Si los puntos coinciden nos salimos

    if (++i >= dim) i = 0;

    nearest(dx > 0 ? root->left : root->right, nd, i, dim, best, best_dist); // Si dx>0, root->left; en caso contrario root->right;
    if (dx2 >= *best_dist) return; // Si la distancia por ese �rbol es peor que la mejor que ten�amos, nos salimos.
    nearest(dx > 0 ? root->right : root->left, nd, i, dim, best, best_dist);
}

#define N 1000000
#define rand1() (rand() / (double)RAND_MAX) //puntos del 0 al 1
#define rand_pt(v) { v.x[0] = rand1(); v.x[1] = rand1(); v.x[2] = rand1(); }
int main(void)
{
    int i;
    struct kd_node_t wp[] = {
        {{2, 3}}, {{5, 4}}, {{9, 6}}, {{4, 7}}, {{8, 1}}, {{7, 2}}
    };
    struct kd_node_t testNode = {{9, 2}};
    struct kd_node_t *root, *found, *million;
    double best_dist;

    root = make_tree(wp, sizeof(wp) / sizeof(wp[1]), 0, 2); //�rbol objetivo, tama�o del �rbol, �ndice inicial, dimensi�n
	//LINEA MIA printf("Num: %d",sizeof(wp) / sizeof(wp[1]));
    visited = 0;
    found = 0;
    nearest(root, &testNode, 0, 2, &found, &best_dist); //nearest devuelve mejor punto as� como mejor distancia

    printf(">> WP tree\nsearching for (%g, %g)\n"
            "found (%g, %g) dist %g\nseen %d nodes\n\n",
            testNode.x[0], testNode.x[1],
            found->x[0], found->x[1], sqrt(best_dist), visited);

    million =(struct kd_node_t*) calloc(N, sizeof(struct kd_node_t)); //calloc es igual que malloc pero inicializa todo a 0
    srand(time(0));
    for (i = 0; i < N; i++) rand_pt(million[i]); //Genera puntos aleatorios

    root = make_tree(million, N, 0, 3); //Crea el kd tree con los puntos generados
    rand_pt(testNode); //Genera un punto aleatorio respecto al que testear

    visited = 0;
    found = 0;
    nearest(root, &testNode, 0, 3, &found, &best_dist); //Devuelve el punto m�s cercano

    printf(">> Million tree\nsearching for (%g, %g, %g)\n"
            "found (%g, %g, %g) dist %g\nseen %d nodes\n",
            testNode.x[0], testNode.x[1], testNode.x[2],
            found->x[0], found->x[1], found->x[2],
            sqrt(best_dist), visited);

    /* search many random points in million tree to see average behavior.
       tree size vs avg nodes visited:
       10      ~  7
       100     ~ 16.5
       1000        ~ 25.5
       10000       ~ 32.8
       100000      ~ 38.3
       1000000     ~ 42.6
       10000000    ~ 46.7              */
    int sum = 0, test_runs = 100000;
    for (i = 0; i < test_runs; i++) {
        found = 0;
        visited = 0;
        rand_pt(testNode);
        nearest(root, &testNode, 0, 3, &found, &best_dist);
        sum += visited;
    }
    printf("\n>> Million tree\n"
            "visited %d nodes for %d random findings (%f per lookup)\n",
            sum, test_runs, sum/(double)test_runs);

    // free(million);

    return 0;
}
