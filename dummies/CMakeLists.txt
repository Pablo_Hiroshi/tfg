cmake_minimum_required (VERSION 2.8 FATAL_ERROR)
set(CMAKE_LEGACY_CYGWIN_WIN32 0)

#----------------------------------------
#        project name

project(tfg)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/kdtree_julius/cmake/")

find_package(Eigen2 REQUIRED)
find_package(LibXml2 REQUIRED)

include_directories(
	${CMAKE_SOURCE_DIR}/kdtree_julius
	${EIGEN2_INCLUDE_DIR}
	${LIBXML2_INCLUDE_DIR}
	)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++11 -Wall")
#------------------------------------------
#        Build targets

#add_executable( 0_Tarea 0_Tarea.cpp)
#add_executable( 1_Tarea 1_Tarea.cpp)
#add_executable( 2_Tarea 2_Tarea.cpp)
#add_executable( kdtreeRosetta kdtreeRosetta.cpp)
#add_executable( DataGen DataGen.cpp)
#add_executable( 2_CompRosetta 2_CompRosetta.cpp)
#add_executable( 2_CompJulius 2_CompJulius.cpp)

set ( SOURCES_FILES Basic_node.hpp Basic_bench.hpp Rosetta_tree.hpp Rosetta_bench.hpp Launch_options.hpp xmlReader.hpp)

add_executable( Tester Tester.cpp ${SOURCES_FILES})
add_executable( 3_Task 3_Task.cpp ${SOURCES_FILES})
add_executable( 4_Task 4_Task.cpp ${SOURCES_FILES})

target_link_libraries(Tester ${LIBXML2_LIBRARIES})
target_link_libraries(3_Task ${LIBXML2_LIBRARIES})
target_link_libraries(4_Task ${LIBXML2_LIBRARIES})
