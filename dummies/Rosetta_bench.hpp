/****************************************************************************
 *Rosetta test bench (for task 3)
*****************************************************************************/

#ifndef _ROSETTA_BENCH_
#define _ROSETTA_BENCH_

#include <math.h>

#include "Basic_node.hpp" 		/* For node */
#include "Rosetta_tree.hpp" 	/* For tree */


/* Returns the pointer to the closest node to the query */
template <class T, int dim>
node<T,dim>* nearest_rosetta(node<T,dim>* root, node<T,dim>* query){
	node<T,dim>* best = 0;
	double best_dist = dist(root, query);
	int i = 0;
	rec_nearest_rosetta(root, query, &best, &best_dist, i);
	return best;
}

/* Recursive algorithm for the nearest search */
template <class T, int dim>
void rec_nearest_rosetta(node<T,dim>* root, node<T,dim>* query,
		node<T,dim>** best, double* best_dist, int i = 0){

	double d, dx, dx2;

    if (!root) return;

    d = dist(root, query);
    dx = root->v[i] - query->v[i];
    dx2 = dx * dx;

    if (!*best || d < *best_dist) {
        *best_dist = d;
        *best = root;

    }

    /* if chance of exact match is high */
    if (!*best_dist) return;

    if (++i >= dim) i = 0;
    rec_nearest_rosetta(dx > 0 ? root->left : root->right, query, best, best_dist, i);
	if (dx2 >= *best_dist) return;
    rec_nearest_rosetta(dx > 0 ? root->right : root->left, query, best, best_dist, i);
}


/* Returns a pointer to a vector with the n closest nodes to the query */
template <class T, int dim>
std::vector <node<T,dim>*> neighbors_rosetta(node<T,dim>* root, node<T,dim>* query, int n = 2){
	std::vector <node<T,dim>*> neighbors_node;
	neighbors_node.reserve(n);
	double max_dist = 0;
	int i = 0;
	int max_idx = 0;
	rec_neighbors_rosetta(root, query, &max_dist, i, neighbors_node, n, &max_idx);
	return neighbors_node;
}

template <class T, int dim>
void rec_neighbors_rosetta(node<T,dim>* root, node<T,dim>* query,
		double* max_dist, int i, std::vector <node<T,dim>*>& neighbors, int n, int* max_idx){

	if (!root) return;

	double d, dx, dx2;
	d = dist(root, query);

	int stored = neighbors.size();
	if(stored < n){
		neighbors.push_back(root);
		if(d > *max_dist){
			*max_dist = d;
			*max_idx = stored;
		}
		stored++;
	}
	else if(d < *max_dist){
		neighbors[*max_idx] = root;
		*max_dist = 0;
		for(int k = 0; k < n; k++){ /*Tag the biggest distance*/
			double aux_dist = dist(neighbors[k], query);
			if( aux_dist > *max_dist){
				*max_dist = aux_dist;
				*max_idx = k;
			}
		}
	}
	/* if chance of exact match is high */
    if (!*max_dist) return;

	dx = root->v[i] - query->v[i];
    dx2 = dx * dx;

    if (++i >= dim) i = 0;
    rec_neighbors_rosetta(dx > 0 ? root->left : root->right, query, max_dist, i, neighbors, n, max_idx);
	if (dx2 > *max_dist && stored == n) return;
    rec_neighbors_rosetta(dx > 0 ? root->right : root->left, query, max_dist, i, neighbors, n, max_idx);
}

/* Returns a pointer to a vector with the n closest nodes to the query */
template <class T, int dim>
std::vector <node<T,dim>*> radius_rosetta(node<T,dim>* root, node<T,dim>* query, const double r){
	std::vector <node<T,dim>*> neighbors_node;
	const double qradius = r*r;
	int i = 0;
	rec_radius_rosetta(root, query, qradius, i, neighbors_node);
	return neighbors_node;
}

template <class T, int dim>
void rec_radius_rosetta(node<T,dim>* root, node<T,dim>* query, const double qradius,
		int i, std::vector <node<T,dim>*>& neighbors){

    if (!root) return;

	double d, dx, dx2;
    d = dist(root, query);
    dx = root->v[i] - query->v[i];
	dx2 = dx*dx;

    if (d < qradius) {
        neighbors.push_back(root);
    }

    if (++i >= dim) i = 0;
	rec_radius_rosetta(dx > 0 ? root->left : root->right, query, qradius, i, neighbors); 
	if (dx2 > qradius) return;
	rec_radius_rosetta(dx > 0 ? root->right : root->left, query, qradius, i, neighbors);

}

#endif
