#ifndef _XMLREADER_
#define _XMLREADER_

#include <vector>
#include <cstdio>
#include <iostream>
#include <sstream>

#include <sys/stat.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include "Basic_node.hpp" 	/* For node */


xmlDocPtr parserDoc(const char *content, int length)
{
    xmlDocPtr doc = xmlReadMemory(content, length, "points.xml", NULL, 0);
    if (doc == NULL)
    {
        fprintf(stderr, "Failed to parse document\n");
        return NULL;
    }
    return doc;
}

template <class T, int dim>
void read_vector( std::istream& reading, std::vector<node<T,dim>>& points)
{
	node<T,dim> point;
	int index = 0;
    while ( reading.good() )
    {
		index++;
        for( int i = 0; i < dim; ++i ) {
			reading >> point.v[i];
		}
		point.data =  index;
		points.push_back(point);
    }
	std::cerr << "Successfully read. Loaded " << index <<" nodes.\n";
}

template <class T, int dim>
void process_points(xmlNodeSetPtr nodes, std::vector<node<T,dim>>& v)
{
    int size = (nodes) ? nodes->nodeNr : 0;
    if ( size == 1 )
    {
        xmlNodePtr p = nodes->nodeTab[0];

        std::string content((const char*)xmlNodeGetContent(p));

        std::stringstream reading(content);

        read_vector( reading, v);
    }
    else
    {
        fprintf(stderr, "Error: Points not found\n");
    }
}

template <class T, int dim>
int find_xpath_expression(xmlDocPtr doc, const xmlChar* xpathExpr, std::vector<node<T,dim>>& v)
{
    xmlXPathContextPtr xpathCtx;
    xmlXPathObjectPtr xpathObj;

    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL)
    {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        return(-1);
    }

    /* Evaluate xpath expression */
    xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
    if(xpathObj == NULL)
    {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
        xmlXPathFreeContext(xpathCtx);
        return(-1);
    }

    // Read points
    process_points( xpathObj->nodesetval, v );

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);

    return(0);
}

template <class T, int dim>
void point_loader( std::vector<node<T,dim>>& v, const char* filename )
{
    FILE* g; char *document; //char *aux;
    struct stat st; size_t size;

    if ( stat(filename, &st) == -1 )
    {
        perror("stat");
        printf("in file: '%s'", filename);
        exit(EXIT_FAILURE);
    }

    stat(filename, &st);
    size = st.st_size;

    g = fopen(filename, "rb");
    document = (char*)malloc( size + 1 );

    size_t result = fread( document, sizeof(char), size, g );
    document[size] = '\0';

    if ( result != size )
    {
        fprintf(stderr,"Error: unable to read the file completely '%s'\n", filename);
    }
    else
    {
        fprintf(stderr,"Info: '%s' reading file completed\n", filename);
    }

    /* Process document VTKFile */
    xmlDoc *doc = NULL;
    xmlNode *root_element = NULL;

    doc = parserDoc(document, size);

    /*Get the root element node */
    root_element = xmlDocGetRootElement(doc);

    find_xpath_expression(doc, (const xmlChar*)"/VTKFile/UnstructuredGrid/Piece/Points/DataArray", v);
	/*
    std::cerr << "V: size = " << v.size() << "\n";
    std::cerr << v[0] << "\n";
    std::cerr << v[v.size()-1] << "\n";
	*/
    xmlFreeDoc(doc);

    xmlCleanupParser();
}



#endif

