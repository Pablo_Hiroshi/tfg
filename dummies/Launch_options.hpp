/****************************************************************************
 * Launching program options (for task 3)
*****************************************************************************/

#ifndef _LAUNCH_OPTIONS_
#define _LAUNCH_OPTIONS_

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>


class launch_options
{
public:
    void load(int argc, char** argv);

	int verbose;
	int printing;
	int output;
	int julius_tests;
	const char* data_file;
	const char* query_file;

	int data_number;
	int query_number;
	int neighbor_number;
	double radius;


};

void launch_options::load(int argc, char** argv)
{
	verbose = 0;
	julius_tests = 0;
	printing = 0;
	output = 1;

	data_file = "iris-spots.vtu";
	query_file = "iris-fem.vtu";
	//data_file = "gear-spots.vtu";
	//query_file = "gear-fem.vtu";
	//data_file = "data.txt";
	//query_file = "query.txt";
	
	data_number = 20;
	query_number = 1;
	neighbor_number = 1;
	radius = 0.025;


    int c;

    struct option my_long_options[] =
    {
        /* These options set a flag. */
        {"verbose", 			no_argument,    &verbose, 1},
		{"printing", 			no_argument,    &printing, 1},
		{"output",     			no_argument,	&output, 1 },
		{"julius_tests",     	no_argument,	&julius_tests, 1 },
		
        //{"brief",   no_argument,       &verbose_flag, 0},
        /* These options don’t set a flag.
           We distinguish them by their indices. */
		{"data_file",     		required_argument, NULL, 'a' },
		{"query_file",     		required_argument, NULL, 'b' },
	    {"data_number",     	required_argument, NULL, 'd' },
		{"query_number",     	required_argument, NULL, 'q' },
		{"neighbor_number",    	required_argument, NULL, 'n' },
        {"radius",     			required_argument, NULL, 'r' },
        {NULL, 0, NULL, 0}
    };

    int option_index = 0;

    while ( (c = getopt_long (argc, argv, "a:b:d:q:n:r:", my_long_options, &option_index) ) != -1 )
    {
        switch (c)
        {
        case 0:
            /* If this option set a flag, do nothing else now. */
            if (my_long_options[option_index].flag != 0)
                break;
            printf ("option %s", my_long_options[option_index].name);
            if (optarg)
                printf (" with arg %s", optarg);
            printf ("\n");
            break;

		case 'a':
            data_file = optarg;
            printf ("data will be read from /%s/\n", data_file);
            break;

		case 'b':
            query_file = optarg;
            printf ("querys will be read from /%s/\n", query_file);
            break;

        case 'd':
            data_number = atoi(optarg);
            printf ("data points number set to %i\n", data_number);
            break;

        case 'q':
            query_number = atof(optarg);
            printf ("query points number set to %i\n", query_number);
            break;

        case 'n':
            neighbor_number = atof(optarg);
            printf ("neighbor points number set to %i\n", neighbor_number);
            break;

        case 'r':
            radius = atoi(optarg);
            printf ("radius set to %f\n", radius);
            break;

        case '?':
            /* getopt_long already printed an error message. */
            break;

        default:
            abort ();
        }
    }

    /* Print any remaining command line arguments (not options). */
    if (optind < argc)
    {
        printf ("non-option ARGV-elements: ");
        while (optind < argc)
            printf ("%s ", argv[optind++]);
        putchar ('\n');
    }
}


#endif
