//Check :D

#ifndef NEARESTNEIGHBORSEARCH_H
#define NEARESTNEIGHBORSEARCH_H

#include "PlaneSplitNode.h"
#include "PointSplitNode.h"

using std::list;

namespace KDTree {

	template <class T, int numAxes>
	class NearestNeighborSearch{
		public:
			typedef Matrix<double, numAxes, 1> Point;
			//Funcion search devuelve una lista de datos
			list<T> search(const PointSplitNode<T, numAxes> tree, Point searchPoint, int numResults = 1) {
				NeighborList<T> results(numResults); //Variable tipo NeighborList con 1 vecino

				recursiveSearch(results, &tree, searchPoint);

				return results.getList();
			};

			//Sobrecarga de SEARCH para PlaneSplitNode
			list<T> search(const PlaneSplitNode<T, numAxes> tree, Point searchPoint, int numResults = 1) {
				NeighborList<T> results(numResults);

				recursiveSearch(results, &tree, searchPoint);

				return results.getList();
			}

		private:
			//Parámetros: NeighborList, puntero al arbol, punto objetivo
			void recursiveSearch(NeighborList<T>& results, const PointSplitNode<T, numAxes>* tree, Point searchPoint, int depth = 0) {
				Point point = tree->getPoint(); //Primer punto del arbol
				double squaredDistance = (point - searchPoint).squaredNorm();
				//Comprobamos si este punto es el nuevo vecino, si es así, almacenamos dato:
				results.testNeighbor(tree->getData(), squaredDistance); 
				
				//Inicializamos variables auxiliares
				PointSplitNode<T, numAxes>* nearChild = 0;
				PointSplitNode<T, numAxes>* farChild = 0;

				int axis = depth % numAxes; //Coordenada cambiante en cada nivel de profundidad
				if (searchPoint[axis] > point[axis]) {//Si el punto objetivo está por encima del punto actual, el posible vecino estará a la dcha
					nearChild = tree->getRight();
					farChild = tree->getLeft();
				} else { //izqda
					nearChild = tree->getLeft();
					farChild = tree->getRight();
				}

				if (nearChild) { //Si hay algo en la dirección intuida:
					recursiveSearch(results, nearChild, searchPoint, depth + 1); //Seguimos buscando
				}

				if (farChild) { //Si hay algo en dirección contraria
					double hyperSphereRadius = searchPoint[axis] - point[axis];
					double radiusSquared = hyperSphereRadius * hyperSphereRadius;
					//Si el punto está más cerca que el ultimo punto
					if (radiusSquared < results.getLargestDistanceSquared()) { //Buscamos por la dirección contraria hasta encontrar un punto más cercano
						recursiveSearch(results, farChild, searchPoint, depth + 1);
					}
				}
			};

			void recursiveSearch(NeighborList<T>& results, const PlaneSplitNode<T, numAxes>* tree, Point searchPoint, int depth = 0) {
				if (tree->getItems().size() > 0) { // leaf node
					typename list<KDTree::Item<T, numAxes> >::const_iterator it;
					int i = 0;
					list<Item<T, numAxes> > items = tree->getItems();
					for(it = items.begin(); it != items.end(); it++) {
						Point point = it->point;
						double squaredDistance = (point - searchPoint).squaredNorm();
						results.testNeighbor(it->item, squaredDistance);
						i++;
					}
				} else { // internal node
					PlaneSplitNode<T, numAxes>* nearChild = 0;
					PlaneSplitNode<T, numAxes>* farChild = 0;

					int axis = tree->getAxis();
					double partition = tree->getPartition();
					if (searchPoint[axis] > partition) {
						nearChild = tree->getRight();
						farChild = tree->getLeft();
					} else {
						nearChild = tree->getLeft();
						farChild = tree->getRight();
					}

					if (nearChild) {
						recursiveSearch(results, nearChild, searchPoint, depth + 1);
					}

					if (farChild) {
						double hyperSphereRadius = searchPoint[axis] - partition;
						double radiusSquared = hyperSphereRadius * hyperSphereRadius;
						if (radiusSquared < results.getLargestDistanceSquared()) {
							recursiveSearch(results, farChild, searchPoint, depth + 1);
						}
					}
				}
			}
	};
}

#endif
