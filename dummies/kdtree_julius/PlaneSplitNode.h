//Check :D

#ifndef PLANESPLITNODE_H
#define PLANESPLITNODE_H

#include <Eigen/Core>
#include <list>

#include "Item.h"

using Eigen::Matrix;
using std::list;

// Esta clase representa un nodo que divide un plano

namespace KDTree {
	template<class T, int numAxes>
		class PlaneSplitNode
		{
			typedef Matrix<double, numAxes, 1> Point; //Point es un tipo de matriz de la librería Eigen
												// <tipo de datos, numero de filas, numero de columnas>
			public:

			// constructor for internal node
			PlaneSplitNode(int axis, double partition, PlaneSplitNode* left = 0, PlaneSplitNode* right = 0): axis(axis), partition(partition), left(left), right(right) {};
			// partition será la coordenada del eje por la cual hemos partido el plano

			// constructor for leaf node
			PlaneSplitNode(list<Item<T, numAxes> > items): items(items) {
				partition = 0.0;
				axis = 0;
				left = 0;
				right = 0;
			}

			/// Return true if this node is a leaf (has no children)
			bool isLeaf() const {
				return (left == 0) && (right == 0);
			}

			PlaneSplitNode* getLeft() const {
				return left;
			}

			PlaneSplitNode* getRight() const {
				return right;
			}

			int getAxis() const {
				return axis;
			}

			double getPartition() const {
				return partition;
			}

			list<Item<T, numAxes> > getItems() const {
				return items;
			}

			private:
			PlaneSplitNode* left;
			PlaneSplitNode* right;

			int axis;
			double partition;

			list<Item<T, numAxes> > items; //lista de la librería estándar de C++.
											//"Casteamos" el tipo de dato del que va a estar compuesto lista. Items es la lista de item
											//Cada item guarda un dato y un punto
		};

}

#endif
