//Check :D

#include <cstdlib>
#include <cstdlib>
#include <fstream>
#include <limits.h>
#include <iostream>
#include <sys/time.h>
#include <vector>

#include "kdtree/PlaneSplitNode.h"
#include "kdtree/PointSplitNode.h"
#include "kdtree/SimpleTreeBuilder.h"
#include "kdtree/MidpointTreeBuilder.h"
#include "kdtree/Printer.h"
#include "kdtree/NearestNeighborSearch.h"

using namespace KDTree;

using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::vector;

typedef Matrix<double, 2, 1> Point; //Puntos de 2 x 1

//Función loadFile, devuelve una lista de items con puntos de R2
list<Item<int, 2> > loadFile(int argc, char* argv[]) //(numero de argumentos, ["loadFile", nombre del archivo, numeros de lineas])
{
	list<Item<int, 2> > items; //Declaramos lista de puntos con datos
	ifstream file(argv[1]); //argv[1]=nombre del archivo
	if (file) {
		int maxlines = 100000000;
		if (argc > 2) { //Si hemos introducido numero de lineas:
			maxlines = atoi(argv[2]); //La segunda cadena del vector será el numero de líneas
		}
		for (int i = 0; i < maxlines; i++) {
			int id;
			double x, y;
			file>>id>>x>>y;
			Item<int, 2> item(id, Point(x, y)); //El archivo está estructurado de la siguiente forma: Dato, Punto (coordenadas)
			if (file.eof()) {
				break;
			}
			items.push_back(item); //Vamos almacenando items
		}
	}

	return items;
}

//(Raiz, lista de puntos)
void findNeighbors(PointSplitNode<int, 2>* root, list<Item<int, 2> > items)
{
	NearestNeighborSearch<int, 2> search; //Declaramos una búsqueda: search
	list<Item<int, 2> >::iterator itemIt;
	//Vamos recorriendo la lista de puntos
	for (itemIt = items.begin(); itemIt != items.end(); itemIt++) {
		cout<<itemIt->item<<" "; //Sacamos el dato por pantalla
		list<int> neighbors = search.search(*root, itemIt->point, 4); //Declaramos una lista de vecinos 
		//Devolvemos 4 vecinos por punto

		list<int>::iterator it = neighbors.begin(); // Recorremos la lista de vecinos y los vamos sacando por pantalla
		it++; // skip first neighbor, it is the point being searched
		for(;it != neighbors.end(); it++) {
			cout<<*it;
			if (it != --neighbors.end()) {
				cout<<",";
			}
		}
		cout<<endl;
	}
}
// Sobrecarga para PlaneSplitNode
void findNeighbors(PlaneSplitNode<int, 2>* root, list<Item<int, 2> > items)
{
	NearestNeighborSearch<int, 2> search;
	list<Item<int, 2> >::iterator itemIt;
	for (itemIt = items.begin(); itemIt != items.end(); itemIt++) {
		cout<<itemIt->item<<" ";
		list<int> neighbors = search.search(*root, itemIt->point, 4);

		list<int>::iterator it = neighbors.begin();
		it++; // skip first neighbor, it is the point being searched
		for(;it != neighbors.end(); it++) {
			cout<<*it;
			if (it != --neighbors.end()) {
				cout<<",";
			}
		}
		cout<<endl;
	}
}

//Cronómetros
void timeNearestNeighbor(PointSplitNode<int, 2>* tree, list<Item<int, 2> > items, const char* name)
{
	struct timeval start; //Una struct timeval tiene un tiempo en segundos y otro en micros
	gettimeofday(&start, 0);

	findNeighbors(tree, items);

	struct timeval end;
	gettimeofday(&end, 0);

	double elapsed = end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec)/1e6; //Obtenemos el tiempo en segundos y micros

	cerr<<"search with "<<name<<" took "<<elapsed<<" seconds."<<endl;
}

//Cronómetros (sobrecarga)
void timeNearestNeighbor(PlaneSplitNode<int, 2>* tree, list<Item<int, 2> > items, const char* name)
{
	struct timeval start; 
	gettimeofday(&start, 0);

	findNeighbors(tree, items);

	struct timeval end;
	gettimeofday(&end, 0);

	double elapsed = end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec)/1e6; 

	cerr<<"search with "<<name<<" took "<<elapsed<<" seconds."<<endl;
}

int main(int argc, char* argv[]) //Main con argumentos
{
	if (argc == 0) {
		return 1;
	}

	list<Item<int, 2> > items = loadFile(argc, argv);
	Printer<int, 2> printer;
	SimpleTreeBuilder<int, 2> simple_builder;
	MidpointTreeBuilder<int, 2> midpoint_builder;

	PointSplitNode<int, 2>* simple_root = simple_builder.build(items);
	PointSplitNode<int, 2>* midpoint_root = midpoint_builder.build(items);
	PlaneSplitNode<int, 2>* simple_plane_split_root;

	timeNearestNeighbor(simple_root, items, "simple tree");
	timeNearestNeighbor(midpoint_root, items, "midpoint tree");

	for (int i = 3; i < 20; i++) {
		simple_plane_split_root = simple_builder.build_plane_split(items, i);
		cerr<<"creating tree with max items "<<i<<endl;
		timeNearestNeighbor(simple_plane_split_root, items, "simple plane split tree");
	}

	return 0;
}
