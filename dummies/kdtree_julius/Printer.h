//Check :D

#ifndef PRINTER_H
#define PRINTER_H

#include <iostream>

using std::endl;
using std::ostream;

#include "PlaneSplitNode.h"
#include "PointSplitNode.h"

namespace KDTree {
	template <class T, int numAxes>
	class Printer {
			typedef Matrix<double, numAxes, 1> Point;
		public:
		//FUNCION PRINT (rama y canal de salida)
		void print(PointSplitNode<T, numAxes>* node, ostream &out) {
			if (node->getLeft()) {
				print(node->getLeft(), out); //Recursividad, empezamos de más al fondo hacia arriba del árbol hasta llegar a la raiz
			}

			print_current(node, out); //Imprimimos el punto (coordenadas y dato) correspondiente a este nivel de profundidad

			if (node->getRight()) {
				print(node->getRight(), out);
			}
		}

		//Sobrecarga para ramas generadas con PlaneSplit
		void print(PlaneSplitNode<T, numAxes>* node, ostream &out) {
			if (node->getLeft()) {
				print(node->getLeft(), out);
			}

			if(node->isLeaf() && node->getItems().size() > 0) {
				list<Item<T, numAxes> > items = node->getItems();
				typename list<Item<T, numAxes> >::iterator it;
				for (it = items.begin(); it != items.end(); it++) {
					out<<it->item<<" ";
					print_point(it->point, out);
				}
				out<<endl;
			} else {
				out<<node->getAxis()<<" "<<node->getPartition()<<endl;
			}

			if (node->getRight()) {
				print(node->getRight(), out);
			}
		}

		private:
		
		void print_current(PointSplitNode<T, numAxes>* node, ostream &out) {
			print_point(node->getPoint(), out); //Imprimimos las coordenadas
			out<<node->getData()<<endl; //Imprimimos el dato almacenado
		}
		//Imprimimos las coordenadas
		void print_point(Point p, ostream &out) {
			out<<"(";
			for (int i = 0; i < numAxes; i++) {
				out<<p[i];
				if (i + 1 < numAxes) {
					out<<", ";
				}
			}
			out<<") ";
		}

	};
}

#endif
