//Check :D

#ifndef KDNODE_H
#define KDNODE_H

#include <Eigen/Core>


#include "NeighborList.h"

using Eigen::Matrix;

namespace KDTree {

	/**
	 * Class PointSplitNode
	 * Represents a single node in a kd-tree
	 * @tparam T	the type of object stored in this node
	 * @tparam numAxes the number of axes (number of dimensions) in the search space
	 */
	template <class T, int numAxes>//class T es cualquier tipo de variable, numAxes es la dimensión de los puntos,
									//especifican los tipos de datos con los que se crearán los objetos
	class PointSplitNode //Clase
	{
		typedef Matrix<double, numAxes, 1> Point; //Point es un tipo de matriz de la librería Eigen
												// <tipo de datos, numero de filas, numero de columnas>
		public:
		/**
		 * Construct a kd-tree node
		 * @param data	the object to store in this nodes
		 * @param point 	the N-dimensional location of this node
		 */
		//Constructores:
		PointSplitNode(T data, Point point): data(data), point(point) { //Inicializan el dato almacenado y el punto en el que está
			left = 0;
			right = 0;
		}

		//Formas de interaccionar con la clase:
		/// Return true if this node is a leaf (has no children)
		bool isLeaf() const {
			return (left == 0) && (right == 0);
		}

		/// Return the data object stored in this node
			//Devuelve el tipo de dato (ver abajo)
		T getData() const {
			return data;
		}

		/// Returns the coordinates of this node
			//Devuelve el punto, recordar que es una matriz 3x1
		Point getPoint() const{
			return point;
		}

		/// Returns the right child pointer of this node
		PointSplitNode* getRight() const {
			//Devuelve el nodo a la derecha
			return right;
		}

		/// Returns the lft child pointer of this node
		PointSplitNode* getLeft() const {
			//Devuelve el nodo a la izqda
			return left;
		}

		/// Set the right child node
		void setRight(PointSplitNode* newNode) {
			//Fija el nodo a la dcha
			right = newNode;
		}

		/// Set the left child node
		void setLeft(PointSplitNode* newNode) {
			//Fija el nodo a la izqda
			left = newNode;
		}

		private:
		/// The object stored in this node
		T data;

		/// This node's left child node
		PointSplitNode* left;

		/// This Node's right child node
		PointSplitNode* right;

		/// The coordinates of this node
		Point point;
	};
}

#endif
