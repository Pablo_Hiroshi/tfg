//Check :D

#ifndef MIDPOINTTREEBUILDER_H
#define MIDPOINTTREEBUILDER_H

#include <Eigen/Core>
#include <limits>
#include <list>

#include "Item.h"
#include "PointSplitNode.h"

using Eigen::Matrix;
using Eigen::Vector3d;
using std::list;

namespace KDTree {
	template <class T, int numAxes>
	class MidpointTreeBuilder { //Otra forma de construir un arbol
		public:
			PointSplitNode<T, numAxes>* build(list<Item<T, numAxes> > items) {
				return recursiveSplit(items);
			}

		private:
			// Función de tipo PointSplitNode*
			PointSplitNode<T, numAxes>* recursiveSplit(list<Item<T, numAxes> > items, int depth = 0) {
				if (items.size() == 0) { //Si no quedan items por cargar hemos terminado
					return 0;
				}

				int axis = depth % numAxes; //Cambio de coordenadas por profundidad

				double midpoint = getMidpoint(items, axis); //Obtenemos el punto medio para la coordenada de este nivel de todos los puntos restantes

				list<Item<T, numAxes> > left; //Almacenamos puntos a la izqda en una lista
				list<Item<T, numAxes> > right; // Dcha
				Item<T, numAxes> middle(0, Vector3d(0, 0, 0));

				partition(items, left, right, middle, midpoint, axis); // Guardamos la coordenada por la que hemos partido

				PointSplitNode<T, numAxes>* node = new PointSplitNode<T, numAxes>(middle.item, middle.point); //Generamos nueva rama desde el punto medio

				node->setLeft(recursiveSplit(left, depth + 1)); //Bajamos un nivel
				node->setRight(recursiveSplit(right, depth + 1)); //Bajamos un nivel

				return node;
			}

		private:
			double getMidpoint(list<Item<T, numAxes> > items, int axis) {
				double high = std::numeric_limits<double>::infinity() * -1;
				double low = std::numeric_limits<double>::infinity();

				typename list<KDTree::Item<T, numAxes> >::iterator it;
				for (it = items.begin(); it != items.end(); it++) {
					if (it->point[axis] < low) {//Vamos acotando hasta encontrado el punto medio (negativo)
						low = it->point[axis];
					}
					if (it->point[axis] > high) {//(positivo)
						high = it->point[axis];
					}
				}
				return (high + low) / 2.0;
			}

			void partition(list<Item<T, numAxes> > items, list<Item<T, numAxes> >& left, list<Item<T, numAxes> >& right, Item<T, numAxes>& middle, const double midpoint, const int axis) {
				typename list<KDTree::Item<T, numAxes> >::iterator it;
				for (it = items.begin(); it != items.end(); it++) {
					if (it->point[axis] < midpoint) { //Si el punto está por debajo del punto medio, lo guardamos a la izqda (al final)
						left.push_back(*it);
					} else {
						//Ordenamos la lista hasta encontrar el punto más a la izqda del lado derecho
						if (right.size() > 0 && it->point[axis] < right.front().point[axis]) {
							right.push_front(*it);
						} else {
							right.push_back(*it); //Si no, al final
						}
					}
				}

				middle = right.front(); //Guardamos el punto más a la derecha como el de enmedio
				right.pop_front(); //eliminamos el punto
			}
	};
}

#endif
