//Check :D

#ifndef SIMPLETREEBUILDER_H
#define SIMPLETREEBUILDER_H

#include <Eigen/Core>

#include "Item.h"
#include "PlaneSplitNode.h"

using Eigen::Matrix;

namespace KDTree {
	template <class T, int numAxes>
	class SimpleTreeBuilder { //Clase para construir un kdtree simple
		public:
			typedef Matrix<double, numAxes, 1> Point;//Point es un tipo de matriz de la librería Eigen
												// <tipo de datos, numero de filas, numero de columnas>

			PointSplitNode<T, numAxes>* build(list<Item<T, numAxes> > items) {
				//función build de tipo PointSplitNode*, parámetros: list. A partir de una lista de item (puntos con un dato almacenado),
				//elabora un kdtree, retorna un puntero a root
				if (items.size() == 0) {
					return 0;
				}
				PointSplitNode<T, numAxes>* root = 0; //Declaramos un PointSplitNode que será la raiz

				typename list<KDTree::Item<T, numAxes> >::iterator it; //iterator 'it' para una lista de items(dato y punto)
				for(it = items.begin(); it != items.end(); it++) {//recorremos la lista de items
					PointSplitNode<T, numAxes>* node = new PointSplitNode<T, numAxes>(it->item, it->point); //Cargamos en una rama un PointSplitNode con los item
					if (root) { //Si estamos en la raiz
						recursiveInsert(root, node); //Bajamos un nivel
					} else {
						root = node;
					}
				}
				return root;
			};

			PlaneSplitNode<T, numAxes>* build_plane_split(list<Item<T, numAxes> > items, unsigned int maxLeafSize) {
				//funcion build_plane_split de tipo PointSplitNode* que genera un arbol a partir de la división en planos
				//recursive_insert es para planos, no confundir con recursiveInsert
				return recursive_insert(items, maxLeafSize); //Introducimos la lista de items(dato con punto), y el máximo numero de ramas finales
			}

		private:
			//Funcion para generar el arbol, los parametros son: raiz y rama
			void recursiveInsert(PointSplitNode<T, numAxes>* root, PointSplitNode<T, numAxes>* node, int depth = 0) {
				int axis = depth % numAxes; //Para ir rotando de coordenada a medida que descendemos en el arbol
				if (node->getPoint()[axis] > root->getPoint()[axis]) {
				//Obtenemos el punto de la raiz y la rama y vemos cual de las coordenadas[i] es mayor
				//Seguimos bajando mientras la rama tenga un valor mayor que la raiz
					if (root->getRight()) { //Si hay algo a en la rama derecha
						recursiveInsert(root->getRight(), node, depth + 1); //Bajamos un nivel hacia la derecha
					} else {
						root->setRight(node); //Si hemos llegado al final de la derecha (no hay más puntos mayores que su raiz inmediatamente superior)
												//Indicamos a la raiz inmediatamente superior cual es su rama derecha
					}
				} else {
					if (root->getLeft()) { //Si hay algo en la rama izquierda
						recursiveInsert(root->getLeft(), node, depth + 1); //Bajamos un nivel hacia la izquierda
					} else {
						root->setLeft(node); //Si hemos llegado al final de la izquierda (no hay más puntos mayores que su raiz inmediatamente superior)
												//Indicamos a la raiz inmediatamente superior cual es su rama izquierda
					}
				}
			}

			//Funcion para generar el arbol, a partir de division del plano
			PlaneSplitNode<T, numAxes>* recursive_insert(const list<Item<T, numAxes> > items, unsigned int maxLeafSize, int depth = 0) {
				if (items.size() <= maxLeafSize) {
					//Si hay menos items que el máximo numero de ramas finales, retornamos un PlaneSplitNode que
					//contiene la lista de items
					return new PlaneSplitNode<T, numAxes>(items);
				} else {
					int axis = depth % numAxes; //Para rotar de coordenda a medida que descendemos en el arbol
					typename list<Item<T, numAxes > >::const_iterator partition_item = items.begin();
					//Declararamos un iterador para la lista de items llamado partition_item y lo inicializamos al ppio.
					double partition;
					list<Item<T, numAxes> > leftItems; //declaramos dos listas
					list<Item<T, numAxes> > rightItems; //declaramos dos listas

					while (leftItems.size() == 0 || rightItems.size() == 0) { //Mientras uno de los dos esté vacío
						partition = partition_item->point[axis]; //Guardamos en partición el punto por el que partimos el plano
						leftItems.clear(); //De la std::list, vaciamos la lista
						rightItems.clear(); //vaciamos la lista
						partition_items(axis, partition, items, leftItems, rightItems); //OJO partition_itemSSSS, llamamos a la funcion
						partition_item++; //Siguiente item
					}
					//Una vez que hemos dividido a cada lado del plano todos los items bajamos por ambos lados
					PlaneSplitNode<T, numAxes>* left = recursive_insert(leftItems, maxLeafSize, depth + 1);
					PlaneSplitNode<T, numAxes>* right = recursive_insert(rightItems, maxLeafSize, depth + 1);
					//Cuando hemos terminado en cada lado subimos un nivel
					//Ahora están guardados los PlaneSplitNode left y right junto con el punto por el que hemos dividido el plano
					return new PlaneSplitNode<T, numAxes>(axis, partition, left, right);
				}
			}

			void partition_items(int axis, double partition, const list<Item<T, numAxes> > items, list<Item<T, numAxes > > &left, list<Item<T, numAxes> > &right) {
				typename list<Item<T, numAxes> >::const_iterator it; //Declaramos iterador de la lista de items
				for(it = items.begin(); it != items.end(); it++) {
					if (it->point[axis] >= partition) { //Si el punto está por encima de la partición o en ella
						right.push_back(*it); //Guardamos en la lista de items la derecha
					} else {
						left.push_back(*it); //izqda
					}
				}
			}

	};
}

#endif
