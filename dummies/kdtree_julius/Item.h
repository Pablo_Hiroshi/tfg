//Check :D

#ifndef ITEM_H
#define ITEM_H

#include <Eigen/Core>


using Eigen::Matrix;

namespace KDTree {
	template <class T, int numAxes>
	struct Item {
		typedef Matrix<double, numAxes, 1> Point; //Point es un tipo de matriz de la librería Eigen 
												// <tipo de datos, numero de filas, numero de columnas>
		Item(T item, Point point): item(item), point(point) {}; //Constructor del item

		T item; //Item de tipo T
		Point point; //punto de 3x1
		bool operator==(const Item<T, numAxes> other) const { //Sobrecarga del operador == ver si un item coincide con otro
			return item == other.item && point == other.point;
		}
	};
}
#endif
