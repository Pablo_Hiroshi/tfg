/****************************************************************************
 *Rosetta code (for task 3)
*****************************************************************************/

#ifndef _ROSETTA_TREE_
#define _ROSETTA_TREE_

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include "Basic_node.hpp" /* For node */



/* Swaps two nodes without changing its place on the tree */
template <class T, int dim>
inline void swap(node<T,dim>* x, node<T,dim>* y) {
    struct node<T,dim> tmp;
	tmp.data = x->data;
	x->data = y->data;
	y->data = tmp.data;
	memcpy(tmp.v, x->v, dim*sizeof(double));
	memcpy(x->v, y->v, dim*sizeof(double));
	memcpy(y->v, tmp.v, dim*sizeof(double));
}

/* Returns the pointer to the median, see quickselect method */
template <class T, int dim>
node<T,dim>* find_median(node<T,dim>* start, node<T,dim>* end, int idx)
{
    if (end <= start) return NULL;
    if (end == start + 1) return start;

    node<T,dim> *p, *store, *md = start + (end - start) / 2;
    double pivot;
    while (1) {
        pivot = md->v[idx];
        swap(md, end - 1);
        for (store = p = start; p < end; p++) {
            if (p->v[idx] < pivot) {
                if (p != store)
                    swap(p, store);
                store++;
            }
        }
        swap(store, end - 1);
		
		/* median has duplicate values */
        if (store->v[idx] == md->v[idx]) //Original
		//if((store->v[idx] == md->v[idx]) && (store->data == md->data))
            return md;

        if (store > md) end = store;
        else        start = store;
    }
}

template <class T, int dim>
node<T,dim>* make_tree(node<T,dim>* t, int len, int i){

	struct node<T,dim>* n;

	if (!len) return 0;
	/* Find the median in order to build balanced trees*/
    if ((n = find_median(t, t + len, i))) {
        i = (i + 1) % dim;
        n->left  = make_tree(t, n - t, i);
        n->right = make_tree(n + 1, t + len - (n + 1), i);
    }
    return n;
}




#endif
