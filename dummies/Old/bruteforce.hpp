/****************************************************************************
 * Funciones empleadas para la generación de archivos con números
 *
*****************************************************************************/
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "kdtree_rosetta/kdtree.hpp"

#ifndef _BRUTEFORCE_
#define _BRUTEFORCE_

using namespace std;

struct node{
	double x[MAX_DIM];
};

//Función que muestra por pantalla un punto:
void shownode(struct kd_node_t *punto)
{
	int i=0;
	for(i=0;i<MAX_DIM;i++){
	cout<<punto->x[i]<<" ";
	}
	cout<<endl;
}

//Sobreacarga: ¡OJO CERR!
void shownode(struct node *punto)
{
	for(int i=0;i<MAX_DIM;i++){
	cerr<<punto->x[i]<<" ";
	}
	cerr<<endl;
}

//Función que genera puntos aleatorios en un archivo
void rnd2file(int num, int range, int dim, char* filename)
{
	int i=0;
	int j=0;
	srand(time(NULL));
	ofstream out(filename);
	for(i=0; i<num; i++){
		for(j=0; j<(dim-1); j++){
		out<<(double)range*rand()/(double)RAND_MAX<<" ";
		}
		out<<(double)range*rand()/(double)RAND_MAX<<endl;
	}
	out.close();
}

//Función que lee los puntos almacenados en un archivo y los genera dinámicamente
struct node* readfile(int num, char* filename)
{
	int i=0;
	int j=0;
	struct node *puntos;
	puntos=new node[num];
	ifstream input(filename);
	if (input.is_open()){
		while ( input.good() && i<num ){
			input>>puntos[i].x[j];
			j++;
			if (j==(MAX_DIM)){j=0;i++;}
		}
	}
	else{
		cout<<"Problema leyendo el archivo\n";
	}
	return puntos;
}

//Sobrecarga de funcion dist (al cuadrado)
    inline double
dist(struct node *a, struct node *b, int dim)
{
    double t, d = 0;
    while (dim--) { //Cada repetición del bucle resta 1 a dim
        t = a->x[dim] - b->x[dim];
        d += t * t;
    }
    return d; // retorna la distancia al cuadrado
}

//Función que traduce puntos a nodos de los kdtree Rosetta
struct kd_node_t* convRosetta(struct node* puntos,int num){
	struct kd_node_t *kd_node;
	kd_node=new kd_node_t[num];
	int i=0;
	int j=0;
	for(i=0; i<num; i++){
		for(j=0; j<MAX_DIM; j++){
			kd_node[i].x[j]=puntos[i].x[j];
		}
	}
	return kd_node;
}

#endif
