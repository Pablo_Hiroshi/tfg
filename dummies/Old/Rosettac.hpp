/****************************************************************************
 *Rosetta code (for task 3)
*****************************************************************************/

#ifndef _ROSETTA_
#define _ROSETTA_

#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include "Brute_force.hpp" /* DIM dist and node */

// Cada punto tiene DIM componentes y guarda cual es su rama izqda y su rama dcha.
struct kd_node{
    struct node point;
    struct kd_node *left, *right;
};

//Funci�n que lee los puntos almacenados en un archivo y los genera din�micamente
struct kd_node* readfile(int num, const char* filename)
{
	int index=0;
	int coord=0;
	struct kd_node *nodes;
	nodes=new kd_node[num];
	std::ifstream input(filename);
	if (input.is_open()){
		while ( input.good() && index<num ){
			input>>nodes[index].point.x[coord];
			coord++;
			if (coord==(DIM)){
				coord=0;
				nodes[index].point.data=index;
				index++;
			}
		}
		std::cerr<<"File "<<filename<<" successfully read\n";
	}
	else{
		std::cerr<<"Problem reading file\n";
	}
	return nodes;
}


// Esta funci�n intercambia las componentes de dos puntos
inline void swap(struct kd_node *x, struct kd_node *y) {
    double tmp[DIM];
	//memcpy copia los bytes tal cual del 2o argumento en el 1ero, el 3er argumento indica el numero de bytes a copiar
    memcpy(tmp,  x->point.x, sizeof(tmp)); //recordar que x es el vector de componente de un punto
    memcpy(x->point.x, y->point.x, sizeof(tmp));
    memcpy(y->point.x, tmp,  sizeof(tmp));
}


// Esta funci�n devuelve la mediana
/* see quickselect method */ //https://en.wikipedia.org/wiki/Quickselect
//https://www.youtube.com/watch?v=MZaf_9IZCrc
struct kd_node* find_median(struct kd_node *start, struct kd_node *end, int idx)
{
    if (end <= start) return NULL;
    if (end == start + 1)
        return start;

    struct kd_node *p, *store, *md = start + (end - start) / 2; //vectores de structs
    double pivot;
    while (1) {
        pivot = md->point.x[idx];//copiamos la componente idx de md en la auxiliar pivot

        swap(md, end - 1); //intercambiamos los valores del pen�ltimo nodo con md
        for (store = p = start; p < end; p++) { //store=start; p=start;
            if (p->point.x[idx] < pivot) { //si el componente de alg�n punto es menor que la mediana
                if (p != store) //y no es igual al valor almacenado previamente
                    swap(p, store); //intercambiamos el punto con el valor almacenado
                store++; //pasamos al siguiente punto
            }
        }
        swap(store, end - 1); //intercambiamos el punto de store con el penultimo punto

        /* median has duplicate values */
        if (store->point.x[idx] == md->point.x[idx]) //si el valor almacenado es el mismo que la mediana retornamos mediana
            return md;

        if (store > md) end = store; //si el valor almacenado es mayor que la mediana el �ltimo punto ser� el almacenado
        else        start = store; //si es menor, el primer punto ser� el almacenado
    }
}

struct kd_node* make_tree(struct kd_node *t, int len, int i, int dim)
{
    struct kd_node *n;

    if (!len) return 0; // ! operador not

    if ((n = find_median(t, t + len, i))) { //Igualamos a n, el puntero que apunta a la mediana
        i = (i + 1) % dim; //1 -> 2 -> 3 vamos rotando en cada nivel del arbol la coordenada respecto a la que ordenamos
		//Recursividad
        n->left  = make_tree(t, n - t, i, dim); //Guardamos en cada punto la rama a la que apunta por la izqda
        n->right = make_tree(n + 1, t + len - (n + 1), i, dim); //Y por la derecha
    }
    return n;
}

/* global variable, so sue me */
int visited;

void nearest_rosetta(struct kd_node *root, struct kd_node *nd, int i, int dim,
        struct kd_node **best, double *best_dist)
{
    double d, dx, dx2;

    if (!root) return; // si no estamos en la raiz, nos salimos (Hay recursividad, va descendiendo)
    d = dist(&root->point, &nd->point, dim); // distancia de la raiz a nd
    dx = root->point.x[i] - nd->point.x[i];
    dx2 = dx * dx;

    visited ++;

    if (!*best || d < *best_dist) { //Si no hay mejor punto o la nueva distancia mejora la ultima mejor
        *best_dist = d; //nueva mejor distancia
        *best = root; //nuevo mejor punto
    }

    /* if chance of exact match is high */
    if (!*best_dist) return; //Si los puntos coinciden nos salimos

    if (++i >= dim) i = 0;

    nearest_rosetta(dx > 0 ? root->left : root->right, nd, i, dim, best, best_dist); // Si dx>0, root->left; en caso contrario root->right;
    if (dx2 >= *best_dist) return; // Si la distancia por ese �rbol es peor que la mejor que ten�amos, nos salimos.
    nearest_rosetta(dx > 0 ? root->right : root->left, nd, i, dim, best, best_dist);
}

#endif
