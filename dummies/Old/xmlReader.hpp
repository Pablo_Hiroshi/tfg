#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

//Función que lee los puntos almacenados en un archivo y los genera dinámicamente
struct node* node3D_loader(int num, const char* filename)
{
	int i;
	xmlDocPtr doc;
	xmlNodePtr cur;
	node* points=new node[num]; // Reservamos memoria
	if (points == NULL) { // Comprobamos que queda memoria
        fprintf(stderr,"out of memory\n");
        return(NULL);
    }
	memset(point, 0, sizeof(node)); //Inicializamos a 0 la memoria reservada
	cur = cur->xmlChildrenNode;
    while (cur != NULL) {
        if ((!strcmp(cur->name, "index")) && (cur->ns == ns))
            points[i].data->name = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        if ((!strcmp(cur->name, "x")) && (cur->ns == ns))
            points[i].x[0] = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		if ((!strcmp(cur->name, "y")) && (cur->ns == ns))
            points[i].x[1] = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		if ((!strcmp(cur->name, "z")) && (cur->ns == ns))
            points[i].x[2] = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
        cur = cur->next;
		++i;
		if(i==num) break;
    }

    return(points);
}