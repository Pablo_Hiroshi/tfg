/****************************************************************************
 * Tarea 2:
 * El objetivo es comparar dos librerías de kd-tree (rosetta y orangejulius)
 * frente al método de fuerza bruta para ver a partir de qué cantidad de números
 * a tratar son más eficientes.
*****************************************************************************/

#include "bruteforce.hpp"
#include <iostream>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <math.h>

/*************************************************************************
* Main debe:
* 	- Obtener el punto más cercano al objetivo así como su distancia
*	- Crear un log con tiempos
*	- Análogo para Rosetta
*	- Mostrar la comparación con pantalla
*************************************************************************/

using namespace std::chrono;

int main(void){

	int i=0;
	int j=0;
	int show=0;
	int num=0;
	double distancia=0;
	double best_distancia=0;
	struct node *puntos;
	struct kd_node_t *kdpuntos,testNode,*bestNode,*root;

	cout<<"Introducir numero de puntos a leer: ";
	cin>>num;

	//Almaceno los datos de un archivo dinámicamente
	puntos=readfile(num,"data.txt");

	cout<<"\nMostrar lectura?[1/0] ";
	cin>>show;
	cout<<endl;
	if (show!=0){
		for(i=0; i<num ;i++){
			for (j=0; j<MAX_DIM; j++){
				if(j==0){
					cout<<i<<": ";
				}
				cout<<puntos[i].x[j]<<" ";
				if(j==2){
					cout<<endl;
				}
			}
		}
	}

	// Convertimos nuestros puntos a los usados por Rosetta
	kdpuntos=convRosetta(puntos, num);
	delete puntos;
	cout<<"Mostrar conversion?[1/0] ";
	cin>>show;
	cout<<endl;
	if (show!=0)
	{
		for (i=0; i<num; i++){
			shownode(&kdpuntos[i]);
		}
	}
	cout<<endl;

	cout<<"Introducir punto query: ";
	for(i=0; i<MAX_DIM;i++){
		cin >> testNode.x[i];
	}
	shownode(&testNode);
	cout<<endl;

	cout<<"METODO FUERZA BRUTA"<<endl;

	best_distancia=dist(&testNode,&kdpuntos[i],MAX_DIM);
	//El objetivo de este "algoritmo" es comparar uno a uno todos los puntos respecto al punto objetivo

	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	for(i=0; i<num; i++){
		distancia=dist(&testNode,&kdpuntos[i],MAX_DIM);
		if(distancia<best_distancia){
			best_distancia=distancia;
			bestNode=&kdpuntos[i];
		}
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration0 = duration_cast<microseconds>( t2 - t1 ).count();
    cout<<"Tiempo empleado en busqueda: "<<duration0<<endl;

	cout<<"El punto mas cercano es: ";
	shownode(bestNode);
	cout<<"a una distancia de "<<sqrt(best_distancia)<<endl;

	cout<<"\nMETODO Kdtree ROSETTA"<<endl;

	visited=0;
	bestNode=0;
	best_distancia=0;

    t1 = high_resolution_clock::now();
	root=make_tree(kdpuntos,num,0, MAX_DIM);
	t2 = high_resolution_clock::now();
	auto duration1 = duration_cast<microseconds>( t2 - t1 ).count();
	cout<<"Tiempo empleado en generar kdtree: "<<duration1<<endl;

	t1 = high_resolution_clock::now();
	nearest(root, &testNode, 0, MAX_DIM, &bestNode, &best_distancia); //nearest devuelve mejor punto así como mejor distancia
	t2 = high_resolution_clock::now();
	auto duration2 = duration_cast<microseconds>( t2 - t1 ).count();
	cout<<"Tiempo empleado en busqueda: "<<duration2<<endl;

    cout<<"El punto mas cercano es: ";
	shownode(bestNode);
	cout<<"a una distancia de "<<sqrt(best_distancia)<<endl;

	// Creación del log:
	ofstream out("log.txt",ofstream::app);
	out<<"--- COMPARACIÓN CON "<<num<<" PUNTOS ---"<<endl;
	out<<"BRUTE FORCE"<<endl;
	out<<"Query time:"<<duration0<<" us"<<endl;
	out<<endl<<"Rosetta Kdtree"<<endl;
	out<<"Kdtree making time: "<<duration1<<" us"<<endl;
	out<<"Query time: "<<duration2<<" us"<<endl;
	if(duration0!=0 && duration1!=0){
		out<<endl<<"Eficiente a partir de: "<<(duration1/duration0)<<" puntos"<<endl;
	}
	out<<endl;
	out.close();

	return 0;
}
