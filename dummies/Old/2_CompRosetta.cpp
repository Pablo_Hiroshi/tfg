/****************************************************************************
 * Tarea2
 * Generador de log.txt con comparativas de kdtreeRosetta frente a bruteforce
*****************************************************************************/

#include <iostream>
#include <chrono>
#include <iomanip>
#include <fstream>
#include <math.h>

#include "bruteforce.hpp"
#include "kdtree_rosetta/kdtree.hpp"

#define NUM_MAX 100000

using namespace std::chrono;

int main(void){

	int i=0;
	int j=0;
	int k=0;

	int error_loops=0;
	int error=0;
	int errorT=0;

	short int aux=0;
	int num=0;
	int loops=0;
	int factor=0;
	int query=0;
	double distancia=0;
	double best_distancia=0;
	double distancia_control=0;
	struct node *puntos, *puntosQ;
	struct kd_node_t *kdpuntos, *testNode, *bestNode, *root;

	double time0=0;
	double time1=0;
	double time2=0;

	cout<<"Introducir numero de repeticiones por calculo: ";
	cin>>loops;
	cout<<"La cantidad de puntos a testear se incrementara en cada bucle por: ";
	cin>>factor;
	cout<<"El numero de querys sera: ";
	cin>>query;

	high_resolution_clock::time_point t1;
	high_resolution_clock::time_point t2;
    // Creación del log:
	ofstream out("logRosetta.txt",ofstream::app);
	out<<"_______________________________________________________"<<endl;

	for(num=10;num<=NUM_MAX;num=num*factor){ //Nube de puntos

		//Almaceno los datos de un archivo dinámicamente
		puntos=readfile(num,"data.txt");
		// Convertimos nuestros puntos a los usados por Rosetta
		kdpuntos=convRosetta(puntos, num);
		delete puntos;
		puntosQ=readfile(query,"query.txt");
		testNode=convRosetta(puntosQ, query);
		delete puntosQ;

		for(i=0;i<query;i++){ //Para cada punto query vemos cuál es su vecino

			for(j=0;j<loops;j++){

				best_distancia=dist(&testNode[0],&kdpuntos[0],MAX_DIM);

				//FUERZA BRUTA
				t1 = high_resolution_clock::now();
				for(k=0; k<num; k++){
					distancia=dist(&testNode[i],&kdpuntos[k],MAX_DIM);
					if(distancia<best_distancia){
						best_distancia=distancia;
						bestNode=&kdpuntos[k];
					}
				}
				t2 = high_resolution_clock::now();
				auto duration0 = duration_cast<microseconds>( t2 - t1 ).count();
				time0+=(double)duration0;

				//ROSETTA KDTREE
				distancia_control=best_distancia;
				visited=0;
				bestNode=0;
				best_distancia=0;
				if(i==1){aux=1;} //Sólo hace falta medir cuánto tarda en hacer el kdtree 1 vez, no hace falta repetirlo para cada punto query
				if(aux==0){
					t1 = high_resolution_clock::now();
					root=make_tree(kdpuntos,num,0, MAX_DIM);
					t2 = high_resolution_clock::now();
					auto duration1 = duration_cast<microseconds>( t2 - t1 ).count();
					time1+=(double)duration1;
				}
				t1 = high_resolution_clock::now();
				nearest(root, &testNode[i], 0, MAX_DIM, &bestNode, &best_distancia); //nearest devuelve mejor punto así como mejor distancia
				t2 = high_resolution_clock::now();
				auto duration2 = duration_cast<microseconds>( t2 - t1 ).count();
				time2+=(double)duration2;

				if(fabs(best_distancia-distancia_control)>1e-6){
					error=1;
				}
			}
			if(error)error_loops++;
			error=0;
		}
		time0=time0/loops;
		time1=time1/loops;
		time2=time2/loops;

		out<<"--- COMPARACION CON "<<num<<" PUNTOS y "<<query<<" QUERYS ---"<<endl;
		out<<"Repeticiones: "<<loops<<endl;
		out<<"BRUTE FORCE"<<endl;
		out<<"Query time for all query points: "<<time0<<" us"<<endl;
		out<<"(Avg query time for 1 query point: "<<(time0/query)<<" us)"<<endl;
		out<<endl<<"Rosetta Kdtree"<<endl;
		out<<"Kdtree making time: "<<time1<<" us"<<endl;
		out<<"Query time for all query points: "<<time2<<" us"<<endl;
		out<<"(Avg query time for 1 query point: "<<(time2/query)<<" us)"<<endl;
		if(time0!=0 && time1!=0){
			out<<endl<<"Eficiente a partir de: "<<(time1*query/time0)<<" puntos";
			if((time1/time0)>1){out<<" NO EFICIENTE :("<<endl;}
			else{out<<" EFICIENTE :)"<<endl;}
		}
		out << endl << "Errores: " << error_loops << endl;
		out<<endl;
		time0=0;
		time1=0;
		time2=0;
		aux=0;
		delete kdpuntos;
		delete testNode;
		errorT+=error_loops;
		error_loops=0;
	}
	out<<endl<< "ERRORES TOTALES: " << errorT <<" de " <<query<<endl;
	out<<"_______________________________________________________"<<endl;
	out.close();
	return 0;
}
