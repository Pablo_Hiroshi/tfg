/****************************************************************************
 * Tarea2
 * Generador de log.txt con comparativas de kdtreeJulius frente a bruteforce
*****************************************************************************/

#include <cstdlib>
#include <fstream>
#include <limits.h>
#include <iostream>
#include <sys/time.h>
#include <vector>
#include <chrono>
#include <math.h>
#include <iomanip>

#include "kdtree_julius/PlaneSplitNode.h"
#include "kdtree_julius/PointSplitNode.h"
#include "kdtree_julius/SimpleTreeBuilder.h"
#include "kdtree_julius/MidpointTreeBuilder.h"
#include "kdtree_julius/Printer.h"
#include "kdtree_julius/NearestNeighborSearch.h"

#include "bruteforce.hpp"

#define DIM 3 //Dimensión 3
#define NUM_MAX 100000
#define DATA_FILE "data.txt"
#define QUERY_FILE "query.txt"

using namespace KDTree;
using namespace std::chrono;

typedef Matrix<double, DIM, 1> Point; //Puntos de 3 x 1

Point node2Eigen (node *punto){
	Point point;
	for(int j = 0; j < DIM; j++){
		 point[j]=punto->x[j];
	}
	return point;
}

//Función loadFile, devuelve una lista de items con puntos de R2
list<Item<int, DIM> > loadFile(int lines, char* filename)
{
	list<Item<int, DIM> > items; //Declaramos lista de puntos con datos
	ifstream file(filename);
	if (file) {
		int maxlines = 100000000;
		if (lines) { //Si hemos introducido numero de lineas:
			maxlines = lines;
		}
		for (int i = 0; i < maxlines; i++) {
			Point x;
			for(int j = 0; j < DIM; j++){
				file >> x[j];
			}
			Item<int, DIM> item(i, x); //El archivo está estructurado de la siguiente forma: Dato, Punto(coordenadas)
			if (file.eof()) {
				break;
			}
			items.push_back(item); //Vamos almacenando items
		}
	}
	return items;
}


int main()
{
	int aux=0;
	int loops=0;
	int factor=0;
	int query=0;
	int num=0;
	
	int error_loops=0;
	int error=0;
	int errorT=0;

	struct node *puntos, *puntosQ;
	int bestBF;

	double bf_querytime=0;
	double kdtree_makingtime=0;
	double kdtree_querytime=0;
	high_resolution_clock::time_point t1;
	high_resolution_clock::time_point t2;

	cout<<"Introducir numero de repeticiones por calculo: ";
	cin>>loops;
	cout<<"La cantidad de puntos a testear se incrementara en cada bucle por: ";
	cin>>factor;
	cout<<"El numero de querys sera: ";
	cin>>query;

	// Creación del log:
	ofstream out("logJulius.txt",ofstream::app);
	out<<"_______________________________________________________"<<endl;

	for(num=10;num<=NUM_MAX;num=num*factor){ //Nube de puntos

		//Bruteforce: Almaceno los datos de un archivo dinámicamente
		puntos=readfile(num, DATA_FILE);
		puntosQ=readfile(query, QUERY_FILE);

		//OrangeJulius:
		list<Item<int, DIM> > items = load_julius(the_points); //Cargamos los puntos
		list<Item<int, DIM> > querys = load_julius(the_querys); //Cargamos los querys
		list<Item<int, DIM> >::iterator querysIt;
		querysIt=querys.begin();
		SimpleTreeBuilder<int, DIM> simple_builder;
		NearestNeighborSearch<int, DIM> search;
		PointSplitNode<int, DIM>* simple_root;

		for(int i=0;i<query;i++){ //Para cada punto query vemos cuál es su vecino
			for(int j=0;j<loops;j++){

				double best_distancia=dist(&puntosQ[0],&puntos[0],DIM);

				//FUERZA BRUTA
				t1 = high_resolution_clock::now();
				for(int k=0; k<num; k++){
					double distancia=dist(&puntosQ[i],&puntos[k],DIM);
					if(distancia<best_distancia){
						best_distancia=distancia;
						bestBF=k;
					}
				}
				t2 = high_resolution_clock::now();
				auto duration = duration_cast<microseconds>( t2 - t1 ).count();
				bf_querytime+=(double)duration;

				//JULIUS
				if(i==1){aux=1;} //Sólo hace falta medir cuánto tarda en hacer el kdtree 1 vez, no hace falta repetirlo para cada punto query
				if(aux==0){
					t1 = high_resolution_clock::now();
					simple_root = simple_builder.build(items); //Construimos el árbol
					t2 = high_resolution_clock::now();
					duration = duration_cast<microseconds>( t2 - t1 ).count();
					kdtree_makingtime+=(double)duration;
				}

				t1 = high_resolution_clock::now();

				list<int> neighbors = search.search(*simple_root, querysIt->point, 1); //Buscamos el vecino

				t2 = high_resolution_clock::now();
				duration = duration_cast<microseconds>( t2 - t1 ).count();
				kdtree_querytime+=(double)duration;

				if(bestBF!=neighbors.front()){
					/* cerr<<"ERROR: Vecinos no coinciden"<<endl;
					cerr<<"Punto: "; shownode(&puntos[bestBF]);
					cerr<<"BruteForce: "<<bestBF<<endl;
					cerr<<"Orange Julius: "<<neighbors.front()<<endl; */
					error=1;
				}
			}
			if(error)error_loops++;
			querysIt++;
			error=0;
		}
		bf_querytime/=loops;
		kdtree_makingtime/=loops;
		kdtree_querytime/=loops;
		out<<"--- COMPARACION CON "<<num<<" PUNTOS y "<<query<<" QUERYS ---"<<endl;
		out<<"Repeticiones: "<<loops<<endl;
		out<<"BRUTE FORCE"<<endl;
		out<<"Query time for all query points: "<<bf_querytime<<" us"<<endl;
		out<<"(Avg query time for 1 query point: "<<(bf_querytime/query)<<" us)"<<endl;
		out<<endl<<"OrangeJulius Kdtree"<<endl;
		out<<"Kdtree making time: "<<kdtree_makingtime<<" us"<<endl;
		out<<"Query time for all query points: "<<kdtree_querytime<<" us"<<endl;
		out<<"(Avg query time for 1 query point: "<<(kdtree_querytime/query)<<" us)"<<endl;
		if(bf_querytime!=0 && kdtree_makingtime!=0){
			out<<endl<<"Eficiente a partir de: "<<(kdtree_makingtime*query/bf_querytime)<<" puntos";
			if((kdtree_makingtime/bf_querytime)>1){out<<" NO EFICIENTE :("<<endl;}
			else{out<<" EFICIENTE :)"<<endl;}
		}
		out << endl << "Errores: " << error_loops << endl;
		out<<endl;
		bf_querytime=0;
		kdtree_makingtime=0;
		kdtree_querytime=0;
		aux=0;
		delete puntos;
		delete puntosQ;
		delete simple_root;
		items.clear();
		querys.clear();
		errorT+=error_loops;
		error_loops=0;
	}
	out<<endl<< "ERRORES TOTALES: " << errorT <<" de " <<query<<endl;
	out<<"_______________________________________________________"<<endl;
	out.close();
	return 0;
}
