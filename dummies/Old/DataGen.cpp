/****************************************************************************
 * DataGen:
 * Generador de puntos aleatorios en un .txt
*****************************************************************************/
#include "bruteforce.hpp"


int main(void){


	int num;
	int range;
	char filename[20];
	cout<<"Introducir numero de puntos a generar: ";
	cin>>num;
	cout<<endl<<"Introducir cota maxima: ";
	cin>>range;
	cout<<endl<<"Nombre del archivo: ";
	cin>>filename;

	//Genero archivo con aleatorios
	rnd2file(num,range,MAX_DIM,filename);
	return 0;
}
