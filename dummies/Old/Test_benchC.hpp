/****************************************************************************
 *Test bench (for task 3)
*****************************************************************************/

#ifndef _TEST_BENCH_
#define _TEST_BENCH_

#define RANGE 300.0

#include "Brute_force.hpp" /* For DIM, node and dist */
#include "Rosetta.hpp" /* For kd_node */


/* Returns the pointer to the closest node to test_node */
struct node* nearest_bruteforce(kd_node *test_node, kd_node *nodes, const int num, const int dim){
	double distance;
	double best_distance = RANGE;
	node *nearest_node;
	for(int j = 0; j<num; j++){
		distance=dist(&test_node->point, &nodes[j].point, dim);
		if(distance < best_distance){
			best_distance=distance;
			nearest_node=&nodes[j].point;
		}
	}
	return nearest_node;
}

/* Returns a pointer to a list with the n closest nodes to test_point */
struct node* neighbors_bruteforce(kd_node *test_node, kd_node *nodes, const int num, const int dim, int n = 2){
	double distance;
	double max_distance = 0;
	int max_index=0;
	node *neighbors_node;
	neighbors_node = new node[n];
	int i = 0;
	for(int j = 0; j < num; j++){
		distance=dist(&test_node->point, &nodes[j].point, dim);
		if(i < n){ /* Fill neighbor vector */
			neighbors_node[i]=nodes[j].point;
			if(distance > max_distance){
				max_distance=distance;
				max_index=i;
			}
			i++;
		}
		else if(distance < max_distance){
			neighbors_node[max_index]=nodes[j].point;
			max_distance=0;
			for(int k = 0; k < n; k++){ /*Tag the biggest distance*/
				double aux_dist=dist(&test_node->point, &neighbors_node[k], dim);
				if( aux_dist > max_distance){
					max_distance=aux_dist;
					max_index=k;
				}
			}
		}
	}
	return neighbors_node;
}

/* Returns a pointer to a list of all the nodes  within the radius r of test_point */
struct node* radius_bruteforce(kd_node *test_node, kd_node *nodes, const int num, const int dim, const double r, int *found){
	double distance;
	const double radius=r*r;
	node *neighbors_node;
	neighbors_node=new node;
	int k=0;
	for(int j = 0; j < num; j++){
		distance=dist(&test_node->point, &nodes[j].point, dim);
		if(distance < radius){
			neighbors_node[k]=nodes[j].point;
			k++;
		}
	}
	if(!k)std::cerr << "0 neighbours found \n";
	*found=k;
	return neighbors_node;
}

#endif
