/****************************************************************************
 * Brute force (for task 3)
*****************************************************************************/

#ifndef _BRUTE_FORCE_
#define _BRUTE_FORCE_

#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

#define DIM 3

struct node {
	int data;
	double x[DIM];
};

//Funcion dist (al cuadrado)
inline
double dist(struct node *a, struct node *b, int dim) {
    double t, d = 0;
    while (dim--) {
        t = a->x[dim] - b->x[dim];
        d += t * t;
    }
    return d;
}


//Funci�n que muestra por pantalla un punto:
void shownode(struct node *point) {
	std::cout << point->data << ": ";
	for(int i=0;i<DIM;i++){
	std::cout << point->x[i] << " ";
	}
	std::cout << "\n";
}


//Funci�n que muestra por pantalla un punto:
void showdist(struct node *point, struct node *query) {
	std::cout << point->data << ": ";
	for(int i = 0; i < DIM; i++){
	std::cout<< point->x[i] << " ";
	}
	std::cout<< "\tDist: " << dist(point, query, DIM);
	std::cout<< "\n";
}

//Funci�n que muestra por pantalla un conjunto de puntos :
void showdist(std::vector <node*> point, struct node *query, int num) {
	for(int i = 0; i < num; i++){
		showdist(point[i], query);
	}
}


#endif
