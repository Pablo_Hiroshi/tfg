#include <chrono>

#include "Basic_node.hpp" 		/* For node */
#include "Rosetta.hpp" 			/* For ros_node */
#include "Test_bench.hpp"
#include "Launch_options.hpp"
//#include "xmlReader.hpp"

#define DIM 3

launch_options options;

int main(int argc, char **argv){

	options.load(argc, argv);
	/* Loading options */
	const int data_number = options.data_number;
	const int query_number = options.query_number;
	const int neigh_number = options.neighbor_number;
	const double radius = options.radius;


	std::vector<node<int, DIM>> the_points;
	std::vector<node<int, DIM>> the_querys;

	readfile(the_points, options.data_file, data_number);
	readfile(the_querys, options.query_file, query_number);
	/*
	std::cout <<"The points: \n";
	showdist(the_points, the_querys[0]);
	*/
	std::cout <<"The querys: \n";
	show(the_querys);
	node<int, DIM>* nearest = nearest_bruteforce(the_points, the_querys[0]);
	std::cout <<"The nearest: \n";
	showdist(*nearest, the_querys[0]);

	std::cout <<"The neighbours: \n";
	std::vector<node<int, DIM>*> the_neighbours = neighbors_bruteforce(the_points, the_querys[0], neigh_number);
	showdist(the_neighbours, the_querys[0]);
	the_neighbours.clear();

	std::cout <<"The sphere: \n";
	the_neighbours = radius_bruteforce(the_points, the_querys[0], radius);
	showdist(the_neighbours, the_querys[0]);
	return 0;
}

