/****************************************************************************
 * Funciones empleadas para descomposición en mantisa y 
 * exponente de números double
*****************************************************************************/
#include <stdio.h>
#include <math.h>
#include <stdlib.h> // para malloc y rand
#include <time.h>

#define SIGN_BIT 0x8000000000000000Lu
#define EXPO_BIT 0x7FF0000000000000Lu
#define BIAS_BIT 0x00000000000003FFLu //1023
#define CAND_BIT 0x000FFFFFFFFFFFFFLu
#define MANT_BIT 0x0010000000000000Lu //El 1 'oculto' de la mantisa normalizada

/****************************************************************************
 * Lattice Modificado y Comentado:
*****************************************************************************/

// Tipo de estructura especial "union" . Tanto 're' como 'i' se guardan en el mismo espacio de memoria
// Sirve para tratar una misma cadena de bits como dos tipos de datos distintos. Ambos ocupan 8 bytes (64 bits)
union number {
    double re;
    long long unsigned i;
};

unsigned lattice(double x, int range)
{
    int exponent = 0;
    double significand = frexp(x, &exponent);
    unsigned result = (unsigned)(floor( significand * exp2( exponent - range )));
    return result;
}

int exponent(double x)
{
    union number u;
    u.re = x;
	// 1 bit de signo + 11 bits de exponente + 52 bits de mantisa = 64
    long long unsigned exponent = (u.i & EXPO_BIT) >> 52Lu;
	// Producto lógico bit a bit, (EXPO_BIT= 0111-1111-1111-(Trece*4 ceros)), ignoramos el primer bit correspondiente al signo
	// y nos quedamos con los 11 siguientes bits correspondiente al exponente, los desplazamos 52 posiciones a la izqda para
	// tener una cadena de bits formateada como un entero
    return exponent - BIAS_BIT - 52Lu; //CAMBIO AQUÍ
	// Los exponentes tienen un 'bias' de 1023. Es decir la cadena de bits del exponente es 1023+exponenteReal
}

long long unsigned bit_significand(double x)
{
    union number u;
    u.re = x;
    long long unsigned result = (u.i & CAND_BIT) + MANT_BIT;
	// Producto lógico bit a bit, (CAND_BIT= 0000-0000-0000-(Trece*4 unos)), ignoramos los 12 primeros bits correspondientes al
	// signo y al exponente y nos quedamos con los 52 bits restantes correspondientes a la mantisa, colocamos un 1 al principio
	// del todo porque (IMPORTANTE:) el IEEE asume que hay un 1 implícito al principio de la mantisa
    return result;
}

/*
double mantissa (double x)
{
	union number u;
    u.re = x;
	u.i = (~u.i & EXPO_BIT) | (u.i & CAND_BIT);
	return u.re;
}
*/

/****************************************************************************
 * /
*****************************************************************************/


// Esta función muestra por pantalla la descomposición en bits de un numero en coma flotante
void dec2bit (long long unsigned dec, int size){

	int i=0;
    long long unsigned aux = SIGN_BIT;
	for (i=0; i<size; i++){
		if (i==0){
			printf("\nSigno:\t\t ");
		}
		if ((dec & aux)== 0){
			printf("0");
		}
		else{
			printf("1");
		}
		if (i==0){
			printf("\nExponente:\t ");
		}
		if (i==11){
			printf("\nMantisa:\t ");
		}
		aux=aux>>1Lu;
	}
	return;
}

// Esta función genera numeros reales aleatorios
number* aleatorios (long long unsigned randoms)
{
	long long unsigned int i=0;
	number* real;
	srand(time(NULL));
	real=new number[randoms];

	for(i=0; i<randoms; i++)
	{
		real[i].re=(double)rand()+((double)rand()/(double)rand());
	}
	return real;
}
