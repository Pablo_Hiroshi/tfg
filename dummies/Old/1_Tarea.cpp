
/****************************************************************************
 * Tarea 1:
 * Repetir tarea 0 midiendo tiempos con C++
*****************************************************************************/

#include "bitsLib.hpp"
#include <iostream>
#include <chrono>
#include <iomanip>
#include <stdint.h>

#define ORD 2
#define ACC 19

using namespace std;
using namespace std::chrono;

int main()
{

	number *real;
	double *numC;
	double *numS;
    unsigned tam=0;
	uint64_t randoms=1;
	int show=0;
	uint64_t i=0;

	cout << "Introducir magnitud de numeros aleatorios a generar(0~10): ";
	cin >> tam;

	for(i=0; i<tam; i++) //Uso un for porque con pow() habría conversión a double
	{
		randoms=ORD*randoms;
	}

	// Generamos números reales aleatorios en memoria dinámica:
	real=aleatorios(randoms);

	cout << "Mostrar numeros aleatorios?[1/0] ";
	cin >> show;

	cout << std::setprecision(ACC) << std::fixed;

	for(i=0; i<randoms; i++)
	{
		if(show==1){cout <<real[i].re<<endl;}
	}

	// Usamos la función estándar de C para descomponer estos números
	int* expC;
	double* mantisaC;
	mantisaC=new double[randoms];
	expC=new int[randoms];


	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for(i=0; i<randoms; i++)
	{
		mantisaC[i]=frexp(real[i].re, &expC[i]);
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( t2 - t1 ).count();
	
	printf("\nTiempo empleado con C:");
    cout << duration;
    cout << " us\n";

	//Salida por pantalla
	cout << "\nC: Mostrar descomposicion?[1/0] ";
	cin >> show;
	if(show==1)
	{
		for(i=0; i<randoms; i++)
		{
			cout <<i<<":\t"<<real[i].re<<"\t = "<<mantisaC[i]<<"* 2^"<<expC[i]<<endl;
		}
	}


	// Usamos las funciones de Santy para descomponer estos números
	int* expS;
	uint64_t* mantisaS;
	mantisaS=new uint64_t[randoms];
	expS=new int[randoms];

    t1 = high_resolution_clock::now();
	for(i=0; i<randoms; i++)
	{
		mantisaS[i] = bit_significand(real[i].re);
		expS[i] = exponent(real[i].re);
	}
	t2 = high_resolution_clock::now();
	duration = duration_cast<microseconds>( t2 - t1 ).count();
	
    cout << "\nTiempo empleado con S:";
    cout << duration;
    cout << " us\n";

	//Salida por pantalla
	cout << "\nS: Mostrar descomposicion?[1/0] ";
	cin >> show;
	if(show==1)
	{
		for(i=0; i<randoms; i++)
		{
			cout <<i<<":\t"<<real[i].re<<"\t = "<<mantisaS[i]<<"* 2^"<<expS[i]<<endl;
		}
	}

	delete real;

	cout << "\nMostrar numeros?[1/0] ";
	cin >> show;
	cout << "\n";
	
	numC=new double[randoms];
	numS=new double[randoms];

	for(i=0; i<randoms; i++)
	{
		numC[i]=mantisaC[i]*pow(2.0,expC[i]);
		numS[i]=mantisaS[i]*pow(2.0,expS[i]);
		if(show==1 && (numC[i]==numS[i]))
		{
		cout <<i<<": "<<"\t"<<"C: "<<numC[i]<<endl
			 << "\tS: "<<numS[i]<<endl;
		}
		else if(numC[i]!=numS[i])
		{
			cerr<<i<<": "<<"\t"<<"C: "<<numC[i]<<endl
			 <<"\tS: "<<numS[i]<<"\tOJO"<<endl;
		}
	}
	

    delete mantisaC;delete expC;
    delete mantisaS;delete expS;
    delete numC;
    delete numS;

	return 0;
}
