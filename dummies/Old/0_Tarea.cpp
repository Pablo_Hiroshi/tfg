/****************************************************************************
 * Tarea 0:
 * Comprobar que nuestras funciones son significativamente mejores que la
 * estándar de C (frexp), mediante la función chronos para una cantidad
 * significativa de números reales
*****************************************************************************/

#include "bitsLib.hpp"
#include <time.h>

int main()
{

	number *real;
	double *numC;
	double *numS;
	unsigned tam=0;
	long long unsigned randoms=1;
	int show=0;
	int i=0;

	printf("Introducir magnitud de numeros aleatorios a generar(0~10): ");
	scanf("%u",&tam);
	printf("Mostrar numeros aleatorios?[1/0] ");
	scanf("%d",&show);

	for(i=0; i<tam; i++) //Uso un for porque con pow() habría conversión a double
	{
		randoms=2*randoms;
	}

	// Generamos números reales aleatorios en memoria dinámica:
	real=(number*)malloc(randoms*sizeof(number));
    srand(time(NULL));
	for(i=0; i<randoms; i++)
	{
		real[i].re=(double)rand()+((double)rand()/(double)rand());
		if(show==1){printf("%10.14f\n", real[i]);}
	}

	// Usamos la función estándar de C para descomponer estos números
	int* expC;
	double* mantisaC;
	mantisaC=(double*)malloc(randoms*sizeof(double));
	expC=(int*)malloc(randoms*sizeof(int));

	clock_t begin = clock();

	for(i=0; i<randoms; i++)
	{
		mantisaC[i]=frexp(real[i].re, &expC[i]);
	}

	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("\nTiempo empleado con C: %.10lf s",time_spent);

	//Salida por pantalla
	printf("\nC: Mostrar descomposicion?[1/0] ");
	scanf("%d",&show);
	if(show==1)
	{
		for(i=0; i<randoms; i++)
		{
			printf("%d:\t %10.14lf\t = %10.14lf * 2^%d\n", i , real[i].re, mantisaC[i], expC[i]);
		}
	}

	// Usamos las funciones de Santy para descomponer estos números
	int* expS;
	long long unsigned* mantisaS;
	mantisaS=(long long unsigned*)malloc(randoms*sizeof(long long unsigned));
	expS=(int*)malloc(randoms*sizeof(int));

	begin = clock();

	for(i=0; i<randoms; i++)
	{
		mantisaS[i] = bit_significand(real[i].re);
		expS[i] = exponent(real[i].re);
	}

	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("\nTiempo empleado con S: %.10lf s",time_spent);

	//Salida por pantalla
	printf("\nS: Mostrar descomposicion?[1/0] ");
	scanf("%d",&show);
	if(show==1)
	{
		for(i=0; i<randoms; i++)
		{
			printf("%d:\t %10.14f\t = %10u * 2^%d\n", i , real[i].re, mantisaS[i], expS[i]);
		}
	}

	printf("\nMostrar discrepancias?[1/0] ");
	scanf("%d",&show);
	if(show==1)
	{
		numC=(double*)malloc(randoms*sizeof(double));
		numS=(double*)malloc(randoms*sizeof(double));

		for(i=0; i<randoms; i++)
		{
			numC[i]=mantisaC[i]*pow(2.0,expC[i]);
			numS[i]=mantisaS[i]*pow(2.0,expS[i]);
            printf("\n%d: %10.14lf =", i,real[i].re);
            printf("\t\tC: %10.14lf\n", numC[i]);
            printf("\t\t\t\t\tS: %10.14lf", numS[i]);
			if(numC[i]!=numS[i])
			{
				printf("\tOJO");
			}
		}
	}
	printf("\n");

	free(mantisaC); free(expC);
	free(mantisaS); free(expS); free(real);
	return 0;
}
