#include "bitsLib.hpp"

#define size 64

int main(){

    union number numero;
    int first=1;
	double mantC;
	int expC;
    while (1){

		first = 1;
        printf("Introducir numero real: ");
        scanf("%lf", &numero.re);
        dec2bit(numero.i, size); // OJO, pongo numero.i no numero.re porque & solo vale para enteros
		mantC = frexp(numero.re, &expC);
        printf("\nExponenteC:\t %d", expC);
		printf("\nMantisaC:\t %10.14lf", mantC);
		printf("\nExponenteS:\t %d", exponent(numero.re));
		printf("\nMantisaS:\t %10.14llu", bit_significand(numero.re));
        printf("\n\n");
    }
    return 0;
}
