/****************************************************************************
 *Julius test bench (for task 3)
*****************************************************************************/

#ifndef _JULIUS_BENCH_
#define _JULIUS_BENCH_

#include "kdtree_julius/PlaneSplitNode.h"
#include "kdtree_julius/PointSplitNode.h"
#include "kdtree_julius/SimpleTreeBuilder.h"
#include "kdtree_julius/MidpointTreeBuilder.h"
#include "kdtree_julius/Printer.h"
#include "kdtree_julius/NearestNeighborSearch.h"


#include "Basic_node.hpp"

using namespace KDTree;


typedef Matrix<double, DIM, 1> Point; //Puntos de 3 x 1

/* Loads the points into julius library format */
template <class T, int dim>
std::list<Item<T, dim> > load_julius(std::vector<node<T,dim>>& points){
	std::list<Item<T, dim> > items;
	int num = points.size();
		for (int i = 0; i < num; i++) {
			Point x;
			for(int j = 0; j < dim; j++){
				x[j] = points[i].v[j];
			}
			Item<T, dim> item(points[i].data, x);
			items.push_back(item);
		}
    return items;
}

template <class T, int dim>
std::list<T> neighbors_julius(PointSplitNode<T, dim>* root, Item<T, dim> query, int neigh_number, int &key)
{
	NearestNeighborSearch<T, dim> search;
	std::list<T> neighbors = search.search(*root, query.point, neigh_number);

	typename std::list<T>::iterator it = neighbors.begin();
	for(;it != neighbors.end(); it++) {
		key+=*it;
	}
	return neighbors;
}

template <class T, int dim>
std::list<T> neighbors_julius(PlaneSplitNode<T, dim>* root, Item<T, dim> query, int neigh_number, int &key)
{
	NearestNeighborSearch<T, dim> search;
	std::list<T> neighbors = search.search(*root, query.point, neigh_number);

	typename std::list<T>::iterator it = neighbors.begin();
	for(;it != neighbors.end(); it++) {
		key+=*it;
	}
	return neighbors;
}

template <class T>
void print_julius(std::list<T> items)
{
	typename std::list<T>::iterator itemIt;
	//Vamos recorriendo la lista de puntos
	for (itemIt = items.begin(); itemIt != items.end(); itemIt++) {
		std::cout << *itemIt <<"\n";
	}
}

#endif
